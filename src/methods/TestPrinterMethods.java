package methods;

import utilities.MathHelper;
import utilities.Printer;

public class TestPrinterMethods {
    public static void main(String[] args) {
        Printer.printGM();

        Printer.helloName("Molly");

        //static vs non-static. Static invoked with class name. Non-static invoked with an object

        Printer myPrinter = new Printer();
        myPrinter.printTechGlobal();

        // return vs void
        System.out.println(MathHelper.sum(3, 5));

Printer.combineString("Hello ", "World");


    }
}
