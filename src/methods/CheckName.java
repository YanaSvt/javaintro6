package methods;

import utilities.ScannerHelper;

public class CheckName {
    public static void main(String[] args) {
        //I can invoke static methods with class name
        //I can call non-static methods with object
        //If you don't see static it means non-static

       // ScannerHelper scannerHelper = new ScannerHelper(); - this part is for non-static

        String name = ScannerHelper.getFirstName();// returns a first name
        //System.out.println(" The name is " + name);

        String lastName = ScannerHelper.getLastName();// returns a last name

        int getAge = ScannerHelper.getAge();// returns an age



        System.out.println(" The full name entered is =  " + name + " " + lastName + " " + getAge);






    }
}
