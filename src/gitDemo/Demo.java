package gitDemo;

public class Demo {
    public static void main(String[] args) {
        String name = "Yana";
        String lastName = "Svt";

        System.out.println(name + lastName);

        String color = "Orange";
        System.out.println("My favorite color is " + color);

        String favoriteCar = "Porsche";
        int year = 2023;
        System.out.println("My favorite car is " + favoriteCar +  " " + year);

    }
}
