package casting;

public class ExplicitCasting {
    public static void main(String[] args) {
        /*
        Explicit casting is Storing bigger data type into smaller data type
        Does not happen automatically. Programmer needs to resolve compiler issue
        Also known as narrowing or down-casting
        LARGE - SMALL
        long -> byte
        int -> short
        double -> float
        double -> int
         */
        long number1 = 542685455; // will be exact only till 127. after it is going left to right in this range -127 to 128
        byte number2 = (byte)number1;// resolving compiler issue here
        System.out.println(number2);


    }

}
