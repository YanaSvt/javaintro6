package casting;

public class CastingCharacters {
    public static void main(String[] args) {
        int i1 = 65;
        char c1 = (char) i1; // explicit char value of i1
        System.out.println(c1);// according to ascii 65 is A

        char c2= 97;
        System.out.println(c2);//a

        char char1 = 'J';//74 decimal values from ascii
        char char2 = 'o';//111
        char char3 = 'h';//104
        char char4 = 'n';//110
        System.out.println(char1 + char2 + char3 + char4); //399 (74+111+104+110)
        System.out.println("" + char1 + char2 + char3 + char4);// because everything after string becomes a string






    }
}
