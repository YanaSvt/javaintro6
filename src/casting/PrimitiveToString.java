package casting;

public class PrimitiveToString {
    public static void main(String[] args) {
        //first method
        int age = 45;
        String agestr = age + "";//did not get why if everything after string becomes a string?
        System.out.println(agestr);

        //second method
        String agestr2 = String.valueOf(age);

    }
}
