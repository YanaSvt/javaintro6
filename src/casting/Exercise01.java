package casting;

public class Exercise01 {
    public static void main(String[] args) {
        int num1 = 5, num2 = 2;

        System.out.println((double)num1/(double)num2);//2.5 or
        System.out.println((double)num1/num2);// also 2.5. If any of them is double answer will be double
    }
}
