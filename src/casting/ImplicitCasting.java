package casting;

public class ImplicitCasting {
    public static void main(String[] args) {
        /*Storing smaller data type into bigger
        Implicit casting happens automatically
        SMALL - LARGE
         byte -> short
         short -> long
         short -> int
         float -> double
         int -> long
         */
        short num1 = 45;
        int num2 = num1;
        System.out.println(num2);
    }
}
