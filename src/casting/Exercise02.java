package casting;

public class Exercise02 {
    public static void main(String[] args) {
        /* phone price is 899.99
        50 per day

         */
        double price = 900;
        double dailySaveAmount = 50;
        System.out.println("You can by the phone after " + (int)(price / dailySaveAmount) + " days.");

    }
}
