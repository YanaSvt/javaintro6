package casting;

public class StringToPrimitives {
    public static void main(String[] args) {
        /*Conversion between string and primitives
        String to primitives


         */
        int num1 = 5, num2 = 10;// because here is no string on the front, numeric
        System.out.println("" + num1 + num2);//10 because of the string on the left it does concatenation - text as well
        System.out.println(""+ (num1 + num2));//15 - text, so if I add anything here it will do concatenation
        // Converting numbers to string
        System.out.println(num1 + "" + num2);//becomes text after string

        System.out.println(String.valueOf(num1) + String.valueOf(num2));//510 as well
        System.out.println(num1 + String.valueOf(num2));//510
        System.out.println(String.valueOf(num2 + num1) + (7 + num2));//"15" + 17 => "1517"

        //Converting string to primitives

        String price = "1597.06";
        System.out.println(Double.valueOf(price) - 10.0); // 1587.06
        System.out.println(Double.parseDouble(price) - 10.0);// same but with better performance. Preferred way





    }
}
