package scannerClass;

import java.util.Scanner;

public class NextintPractice {
    public static void main(String[] args) {

        System.out.print("\n====================Exercise====================\n");

        Scanner scanner = new Scanner(System.in);
        int num1, num2, num3;

        System.out.println("Please enter number 1");
        num1 = scanner.nextInt();

        System.out.println("please enter number 2");
        num2 = scanner.nextInt();

        System.out.println("Please enter number 3");
        num3 = scanner.nextInt();

        System.out.println("The sum of the numbers you entered is " + (num1 + num2 + num3));



    }
}
