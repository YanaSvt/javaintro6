package scannerClass;

import java.util.Scanner;

public class ScannerFirstAndLastname {
    public static void main(String[] args) {

        Scanner nameinput = new Scanner(System.in);
        String fName, lName, address;
                                                            //next and nextline methods
       System.out.println("Please enter your First name?");
        fName = nameinput.next();

        System.out.println("Please enter your Last name?");
        lName = nameinput.next();
        nameinput.nextLine(); // fixing a bug

        System.out.println("Please enter your address");
        address = nameinput.nextLine();

        System.out.println("Your full name and address is " + fName + " " + lName + " " + address);

        //nextint method

        System.out.println("Please enter a number");
        int number = nameinput.nextInt();
        System.out.println("The number you chose is  " + number);



    }
}
