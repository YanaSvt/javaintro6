package primitives;

import java.sql.SQLOutput;

public class AbsoluteNumbers {
    public static void main(String[] args) {
        /* System.out.print("\n----------------TASK-1-BYTE---------------\n");
        byte myNumber = 45;
        System.out.println(myNumber); //45

        System.out.println("The max value of byte = " + Byte.MAX_VALUE); //127
        System.out.println("The min value of byte = " + Byte.MIN_VALUE); //-128

        System.out.print("\n----------------TASK-2-SHORT---------------\n");
        short numberShort = 150;
        System.out.println(numberShort); //150
        System.out.println("The max value of short = " + Short.MAX_VALUE); //32767
        System.out.println("The min value of short = " + Short.MIN_VALUE); //-32768

        System.out.print("\n----------------TASK-3-INT---------------\n");
        int myInteger = 20000000;
        System.out.println("The max value of int = " + Integer.MAX_VALUE); // 2147483647
        System.out.println("The min value of int = " + Integer.MIN_VALUE); // -2147483648

        System.out.print("\n----------------TASK-4-LONG---------------\n");
        long myBigNumber = 2147483648L;
        System.out.println("The max value of long = " + Long.MAX_VALUE); // 9223372036854775807
        System.out.println("The min value of long = " + Long.MIN_VALUE); // -9223372036854775808 */

        System.out.print("\n----------------TASK-5-long more info---------------\n");
        long l1 = 5;
        long l2 = 23_875_283_764_827_364L; //we need to put L when the number is more than the capacity int

        System.out.println(l1); //5
        System.out.println(l2); //23875283764827364





    }
}
