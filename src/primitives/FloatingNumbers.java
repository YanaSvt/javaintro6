package primitives;

public class FloatingNumbers {
    public static void main(String[] args) {
        System.out.println("\n----------------floating numbers---------------\n");
        float myFloat1 = 20.5F;
        double myDouble1 = 20.5;

        System.out.println(myFloat1); //20.5
        System.out.println(myDouble1); //20.5

        System.out.println("\n----------------floating numbers more---------------\n");
        float myFloat2 = 10;
        double myDouble2 = 234235;

        System.out.println(myFloat2); //10.0
        System.out.println(myDouble2); //234235.0

        System.out.println("\n----------------floating numbers precision---------------\n");
        float f1 = 3274.4546464646464F;
        double d1 = 3274.56565655656565;

        System.out.println(f1);
        System.out.println(d1);

    }
}
