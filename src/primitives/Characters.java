package primitives;

public class Characters {
    public static void main(String[] args) {
        System.out.print("\n----------------char---------------\n");
        char c1 = 'A';
        char c2 = ' ';
        System.out.println(c1);// A
        System.out.println(c2);// space

        char myFavChar = '*';
        System.out.println("My favorite char = " + myFavChar);//My favorite char = *
    }
}
