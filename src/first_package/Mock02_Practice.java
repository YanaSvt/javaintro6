package first_package;

import utilities.ScannerHelper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.TreeSet;


public class Mock02_Practice {
    public static void main(String[] args) {
        System.out.print("\n====================TASK1====================\n");
        System.out.println(middleThree("CAPTAIN"));//print 3 middle letter in odd string

        System.out.print("\n====================TASK2====================\n");
        System.out.println(frontAgain("cambuca"));//return true if string starts an ends with same 2 characters



        System.out.print("\n====================TASK4====================\n");
        System.out.println(middleTwo("CAPTAI"));//print 2 middle letter in even string

        System.out.print("\n====================TASK5====================\n");
        System.out.println(reverse("CAPTAI"));//reversed string

        System.out.print("\n====================TASK6====================\n");
        System.out.println(checkPalindrome("civic"));//palindrome

        System.out.print("\n====================TASK7====================\n");
        System.out.println(fibonacci(9));//0 1 1 2 3 5 8 13 21  fibonacci

        System.out.print("\n====================TASK8====================\n");
        countChars(ScannerHelper.getString());//count chars upper lower

        System.out.print("\n====================TASK9====================\n");
        countVowels("Yana Svet");//count vowels

        System.out.print("\n====================TASK10====================\n");
        String[] arr = {"Scooby Doo", "Snoopy", "Blue", "Pluto", "Dino", "Sparky"};
        containsPluto(arr);//contains pluto

        System.out.print("\n====================TASK11====================\n");
        String[] arr2= {"Pen", "notebook", "Book", "paper", "bag", "pencil", "Ruler"};
        countInArray(arr2);// uppercase lowercase contains

        System.out.print("\n====================TASK12====================\n");
        int[] arr3 = {5, 8, 13, 1, 2};
        int[] arr4 = {9, 3, 67, 1, 0};
        ArrayfromArrays(arr3, arr4);//create new array with max from 2 arrays

        System.out.print("\n====================TASK13====================\n");
        int[] nums = {1,2,3,4};
        swapEnds(nums);//swap ends in array

        System.out.print("\n====================TASK14====================\n");
        ArrayList <String> list = new ArrayList(Arrays.asList("Desk", "Laptop", "Mouse", "Monitor", "Mouse-Pad", "Adapter"));
        arrayListCounts(list);//ArrayList counts contains starts ends

        System.out.print("\n====================TASK15====================\n");
        ArrayList <String> kitchen = new ArrayList(Arrays.asList("Plate", "spoon", "fork", "Knife", "cup", "Mixer"));
        arrayListchars(kitchen);//upper lower contains starts ends




    }
    //TASK01 //print 3 middle letter in odd string
    public static String middleThree(String str) {
        return str.substring(str.length()/2-1, str.length()/2 + 2);
    }

    //TASK02 return true if string starts an ends with same 2 characters
    public static boolean frontAgain(String str){
        if (str.length() < 2) return false;
        else return (str.substring(0,2).equals(str.substring(str.length()-2)));
    }


    //TASK04 - print 2 middle letter in even string
    public static String middleTwo(String str){
        return str.substring(str.length()/2-1, str.length()/2+1);
    }

    //TASK05 -- reversed string
    public static String reverse(String str){
        String reversed = "";
        for(int i = str.length()-1; i >= 0; i--){
            reversed += str.charAt(i);
        }
        return reversed;
    }

    //TASK06
    public static String checkPalindrome(String str){
       // Scanner input = new Scanner(System.in);
       // System.out.println("Please enter a word");
      //  String word  = input.nextLine();
        String reversedName = "";

        for(int i = str.length()-1; i >= 0; i--){
            reversedName += str.charAt(i);
        }
        if (str.length()<1 || str.equals(" ")) return "This word does not have 1 or more characters";
        else if (str.equals(reversedName)) return "This word is palindrome";
        else return "This word is not palindrome";
    }

   // System.out.println("\n---------2nd way--------\n");
 //   String str2 = ScannerHelper.getString();
   //     System.out.println(str2.split(" ").length);

        //TASK07
    public static String fibonacci(int fib){
        String answer = "";
        int first = 0;
        int second = 1;

        for (int i = 0; i < fib; i++) {
            answer += first + " ";
            int nextNum = first + second;
            first = second;
            second = nextNum;
        }
        return answer;
    }

    //TASK08
    public static void countChars(String str){
        int _letters = 0, _lower = 0, _upper = 0, _digits = 0, _spaces = 0, _specials = 0;

        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);

            if(Character.isUpperCase(c)) {
                _upper++;
                _letters++;
            }
            else if(Character.isLowerCase(c)) {
                _lower++;
                _letters++;
            }
            else if(Character.isDigit(c)) _digits++;
            else if(Character.isWhitespace(c)) _spaces++;
            else _specials++;
        }

        System.out.println("Letters = " + _letters);
        System.out.println("Uppercase letters = " + _upper);
        System.out.println("Lowercase letters = " + _lower);
        System.out.println("Digits = " + _digits);
        System.out.println("Spaces = " + _spaces);
        System.out.println("Specials = " + _specials);
    }

    //TASK09
    public static void countVowels(String str){
        int count = 0;

        for (int g = 0; g <= str.length()-1; g++) {
            if(str.toLowerCase().charAt(g) == 'a' || str.toLowerCase().charAt(g) == 'e' || str.toLowerCase().charAt(g) == 'i' || str.toLowerCase().charAt(g) == 'o' || str.toLowerCase().charAt(g) == 'u') count++;
        }
        if (count == 1) System.out.println("There is " + count + " vowel letter in this full name");
        else System.out.println("There are " + count + " vowel letters in this full name");
    }

    //TASK10
    public static void containsPluto(String[] arr){
        Arrays.sort(arr);
        System.out.println(Arrays.toString(arr) + "\n" + Arrays.asList(arr).contains("Pluto"));
    }

    //TASK11
    public static void countInArray(String[] arr2){
        int UCase = 0, LCase = 0, startsWithBorP = 0, containsBookPen = 0;

        for (String s : arr2) {
            if(Character.isUpperCase(s.charAt(0)))UCase++;
            else LCase++;

            if(s.toLowerCase().charAt(0) == 'b' || s.toLowerCase().charAt(0) == 'p')startsWithBorP++;
            if(s.toLowerCase().contains("book") || s.toLowerCase().contains("pen"))containsBookPen++;
        }
        System.out.println(Arrays.toString(arr2));
        System.out.println("Elements starts with uppercase = " + UCase);
        System.out.println("Elements starts with lowercase = " + LCase);
        System.out.println("Elements starts with B or P = " + startsWithBorP);
        System.out.println("Elements having ”book” or “pen”= " + containsBookPen);
    }
    //TASK12
    public static void ArrayfromArrays(int[] arr3, int [] arr4){
        int[] maxArr = new int[arr3.length];

        for (int i = 0; i < maxArr.length; i++) {
            maxArr[i] = Math.max(arr3[i], arr4[i]);
        }

        System.out.println("1st array is = " + Arrays.toString(arr3));
        System.out.println("2nd array is = " + Arrays.toString(arr4));
        System.out.println("3rd array is = " + Arrays.toString(maxArr));

    }
    //TASK13 //swap ends in array
    public static void swapEnds(int[] nums){
        int temp = nums[0];
        nums[0] = nums[nums.length-1];
        nums[nums.length-1] = temp;
        System.out.println(Arrays.toString(nums));
    }

    //TASK14
    public static void arrayListCounts(ArrayList<String> list){
        System.out.println(list);
        Collections.sort(list);
        System.out.println(list);

        int countM = 0;
        for (String str : list) {
            if (str.toLowerCase().contains("m"))
                countM++;
        }
        System.out.println(countM);

        int noAE = 0;
        for (String str2: list) {
            if (!str2.toLowerCase().contains("a") && !str2.toLowerCase().contains("e"))
                noAE++;
        }
        System.out.println(noAE);
    }
    //TASK15
    public static void arrayListchars(ArrayList<String> kitchen){
        int upper = 0;
        int lower = 0;

        for (String kitch: kitchen) {
            char c = kitch.charAt(0);
            if(Character.isUpperCase(c)) upper++;
            else if(Character.isLowerCase(c)) lower++;
        }
        System.out.println("Elements starts with uppercase = " + upper);
        System.out.println("Elements starts with lowercase = " + lower);

        int countP = 0;
        for (String k : kitchen) {
            if (k.toLowerCase().contains("p")) countP++;
        }
        System.out.println("Elements having P or p = " + countP);

        int startEndP = 0;
        for (String kit : kitchen) {
            if (kit.toLowerCase().startsWith("p") || kit.toLowerCase().endsWith("p")) startEndP++;
        }
        System.out.println("Elements starting or ending with P or p = " + startEndP);
    }






}
