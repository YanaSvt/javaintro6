package first_package;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Mock03_Practice {

    //Count how many consonants are in the String
    public static int countConsonant(String str){
        str = str.replaceAll("[aeiouAEIOU]", "");//removing all vowels from the string
        return str.length();
    }
    //biggest difference in int Array
    public static int biggestDifference(int[] num){
        int min =num[0];
        int max =num[0];
        for(int i=1; i < num.length; i++) {
            min = Math.min(min,num[i]);
            max = Math.max(max,num[i]);
        }
        return max - min;
    }
    //remove dupes from Arraylist and return new ArrayList
    public static ArrayList<Integer> removeDupes(ArrayList<Integer> list){
        ArrayList<Integer> newList = new ArrayList<>();
        for (int l : list) {
            if(!newList.contains(l)) newList.add(l);
        }
        return newList;
    }


    public static void main(String[] args) {

 System.out.print("\n====================Count how many consonants are in the String====================\n");
        System.out.println(countConsonant("Hello"));

        System.out.print("\n====================counting vowels(without regex)====================\n");
        String st = "Hello, my name is bilal and i go to techGlobal";

        int vowelCounter = 0;
        for (char c : st.toCharArray()) {
            if(Character.toLowerCase(c) == 'a' || Character.toLowerCase(c) == 'e' || Character.toLowerCase(c) == 'o'
                    || Character.toLowerCase(c) == 'i' || Character.toLowerCase(c) == 'u'){
                vowelCounter++;
            }
        }
        System.out.println("The word has " + vowelCounter + " vowels");

        System.out.print("\n====================counting vowels with regex====================\n");
        //str = str.replaceAll("[^aeiouAEIOU]", "");//Apple -> Ae
        //System.out.println("The word has " + str.length() + " vowels");

        System.out.print("\n====================Count words====================\n");
        String sentence = "hello,  my name is john.";
        String[] arr = sentence.split(" ");// ["hello,", "", "my", "name", "is", "john."]

        System.out.print("This sentence contains " + arr.length + " words");
        for (String s : arr) {
            System.out.println(s);
        }


        System.out.print("\n==========REGEX WAY count words===========\n");

        Pattern pattern = Pattern.compile("[a-zA-Z]{1,}");
        Matcher matcher = pattern.matcher("hello,  my name is john.");
        int wordCount = 0;

        while(matcher.find()){
            System.out.print(matcher.group());
            wordCount++;
        }
        System.out.println("This sentence contains " + wordCount + " words");

        System.out.print("\n==========count evens in Array===========\n");
        int[] nums = {2, 1, 2, 3, 4};
        int count=0;
        for(int n : nums) {
            if(n%2==0) count++;
        }
        System.out.println(count);

        System.out.print("\n==========Biggest difference in Array===========\n");
        System.out.println(biggestDifference(new int[] {2, 1, 2, 3, 5}));


        System.out.print("\n==========Count xx in a string===========\n");
        String strXX = "abcxx";
        int countXX = 0;
        for (int i = 0; i < strXX.length()-1; i++){
            if(strXX.substring(i,i+2).equals("xx")) countXX++;
        }
        System.out.println(countXX);

        System.out.print("\n==========Count A in a string===========\n");
       String test =  "TechGlobal is a QA bootcamp";
        int counter = 0;
        for (char c : test.toCharArray()) {
            if (Character.toLowerCase(c) == 'a') counter++;
        }
        System.out.println(counter);

        System.out.print("\n==========Count positive elements in ArrayList===========\n");
        ArrayList<Integer> list = new ArrayList <> (Arrays.asList(-45, 0, 0, 34, 5, 67));
        int c = 0;
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i) > 0) c++;
        }
        System.out.println(c);

        System.out.print("\n==========Remove duplicates from Int ArrayList and print new ArrayList without dupes===========\n");
        System.out.println(removeDupes(new ArrayList<> (Arrays.asList(10, 20, 35, 35, 60, 70, 60))));




        /*
        same for String:
         ArrayList<String> newList = new ArrayList<>();
        for (String s : list) {
            if(!newList.contains(s)) newList.add(s);
        }
        return newList;
         */

        System.out.print("\n==========Remove extra spaces from String===========\n");
        String withSpaces = "   I   am      learning     Java      ";
        System.out.println(withSpaces.trim().replaceAll("[\\s]+", " "));

        System.out.println("\n-----------Count words 2-------------\n");
        String strr = "    I     like     Java      ";
        System.out.println(strr.trim().split("\\s+").length); // 3


    }
}
