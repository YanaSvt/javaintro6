package strings;

import com.sun.xml.internal.ws.addressing.WsaActionUtil;

public class UnderstandingStrings {
    public static void main(String[] args) {
        String s1;//declaring of s1 as a String
        s1 = "Techglobal school";//initializing a value

        String s2 = " is the best";//declaring and initializing s2
        System.out.println(s1);
        System.out.println(s2);

        System.out.print("\n----------------Concat using + ---------------\n");
        String s3 = s1 + s2;// or s1 + " " + s2
        System.out.println(s3);//Techglobal school is the best in one lane

        System.out.print("\n----------------Concat method---------------\n");
        String s4 = s1.concat(s2);// or String s4 = s1.concat(str: " ").concat(s2);
        System.out.println(s4);//Techglobal school is the best

        System.out.print("\n----------------Concat Exercise 3---------------\n");
        String wordPar1 = "le";
        String wordPart2 = "ar";
        String wordPart3 = "ning";
        String wholeWord = wordPar1 + wordPart2 + wordPart3;
        System.out.println(wholeWord);//learning




    }
}
