package practices;

import utilities.ScannerHelper;

public class Exercise01_StringMethods {
    public static void main(String[] args) {

        System.out.print("\n====================TASK1====================\n");
        String str = ScannerHelper.getString();
        System.out.println("The string given is = " + str);

        System.out.print("\n====================TASK2====================\n");
        /*
        Check if the given string has at least one character (length 1)
        If the length is 0 print "The string given is empty.
        If it has some characters then print the length of the string w the message "The length is = {}
         */

        //first way
       if (str.length() < 1) System.out.println("The string given is empty");
       else System.out.println("The length is = " + str.length());

        //second way
       // if(str.isEmpty()) System.out.println("The string given is empty");
       // else System.out.println("The length is = " + str.length());

        //third way
        //if(str.equals("")) System.out.println("The string given is empty");
        // else System.out.println("The length is = " + str.length());

       //Turnery way
        //System.out.println(str.isEmpty() ? "The string given is empty" : "The length is = " + str.length());

        System.out.print("\n====================TASK3====================\n");
        /*
        If it is not empty print the first character with message "The first character =  {},
        if it is empty "There is no character in this String"
         */

      if (str.isEmpty()) System.out.println("There is no character in the sString");
       else System.out.println("The first character = " + str.charAt(0));

        System.out.print("\n====================TASK4====================\n");
        /*
        Print last character if not empty
         */

        if (str.isEmpty()) System.out.println("There is no character in the sString");
        else System.out.println("The last character = " + str.charAt(str.length()-1));

        System.out.print("\n====================TASK5====================\n");
       /*
    -Check if the String contains any vowel letters
    -if it has any vowel, then print "This String has vowel"
    -Else, print "This String does not have vowel"
    Vowels = a, e, i, u, o, A, E, I, U, O

    Hello -> This String has vowel
    bcd   -> This String does not have vowel
    "  "  -> This String does not have vowel
 */
        if (str.toLowerCase().contains("a") || str.toLowerCase().contains("e") || str.toLowerCase().contains("i") || str.toLowerCase().contains("u") || str.toLowerCase().contains("o"))
            System.out.println("This String has vowel");
        else System.out.println("This String does not have vowel");







    }

    }

