package mathClass;

public class FindSquareRoot {
    public static void main(String[] args) {
        // 5 ^ 2 == 25
        // Square root of 25 == 5

        System.out.println(Math.sqrt(25));//returns floating number
        System.out.println(Math.sqrt(30-5));//returns floating number 5
        //Negative numbers don't work here

        System.out.println(Math.round(5.5));

    }
}
