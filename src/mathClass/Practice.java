package mathClass;

import java.util.Scanner;

public class Practice {
    public static void main(String[] args) {
        /*System.out.print("\n====================TASK1 ====================\n");
        int randomNumber = (int) (Math.random() * 51);
        //int randomNumber2 = (int) Math.round(Math.random()*50);
        int result = randomNumber * 5;
        //for (int i = 0; i < 1000 b)
        System.out.println("The random number * 5 = " + result);
        //System.out.println("The random number * 5 = " + Math.round(Math.random()*50));

        System.out.print("\n====================TASK2====================\n");
        //int random1  = (int)(Math.round(Math.random() * 9) + 1);// this is correct
        int random1 = (int) (Math.random() * 10 +1);
        System.out.println(random1);
        //int random2 = (int)(Math.round(Math.random() * 10));
        int random2 = (int) (Math.random() * 10 +1);
        System.out.println(random2);


        System.out.println("Min number = " + Math.min(random1, random2));
        System.out.println("Max number = " + Math.max(random1, random2));
        System.out.println("Difference = " + Math.abs(random1 - random2));

        System.out.print("\n====================TASK3====================\n");

        int ranDo = (int) (Math.random() * 51 + 50); // will give 0 to 50 / 50 to 100 or round 50 + 50
        System.out.println(ranDo);
        System.out.println("The random number % 10 = " + ranDo % 10);  */

        System.out.print("\n====================TASK4====================\n");
        Scanner input = new Scanner(System.in);
        System.out.println("Please enter 5 numbers between 1 to 10 (1 and 10 are included");
        int input1 = input.nextInt();
        int input2 = input.nextInt();
        int input3 = input.nextInt();
        int input4 = input.nextInt();
        int input5 = input.nextInt();
        System.out.println((input1 * 5) + (input2 * 4) + (input3 * 3) + (input4 * 2) + input5);


    }
}
