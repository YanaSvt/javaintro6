package mathClass;

public class FindAbs {
    public static void main(String[] args) {
        System.out.print("\n====================Absolute ====================\n");
        // -35 -> |-35| == 35

        int num = -100;
        System.out.println(Math.abs(num));//converts negative number to positive
        System.out.println(Math.abs(50-55));//5
        System.out.println(Math.abs(5)); //for positive will be the same number 5
        System.out.println(Math.abs(-87.999999998776)); //87.999999998776
    }
}
