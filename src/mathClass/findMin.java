package mathClass;

import java.util.Scanner;

public class findMin {
    public static void main(String[] args) {
        System.out.print("\n====================The min of 2 ====================\n");
        Scanner input = new Scanner(System.in);
        System.out.println("Please enter 2 numbers");
        int num1 = input.nextInt();
        int num2 = input.nextInt();

        System.out.println("The min of the given numbers is " + Math.min(num1,num2));

        System.out.print("\n====================The min of 3 ====================\n");
        System.out.println("Please enter 3 numbers");
        int number1 = input.nextInt();
        int number2 = input.nextInt();
        int number3 = input.nextInt();

        System.out.println("The min of " + number1 + ", " + number2 + ", " + number3 + " is: " + Math.min(Math.min(number1, number2), number3 ));


    }
}
