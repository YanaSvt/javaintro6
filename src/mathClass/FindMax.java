package mathClass;

public class FindMax {
    public static void main(String[] args) {
        System.out.print("\n====================The max of 2 numbers====================\n");
        int num1 = 10, num2 = 15, max = Math.max(num1, num2);
        System.out.println(max);

        System.out.print("\n====================The max of 4 numbers====================\n");
        int number1 = 2, number2 = 8, number3 = 5, number4 = 18;
        //max between 1 and 2 is 8, max between 3 and4 is 18
        int max1 = Math.max(number1, number2);
        int max2 = Math.max(number3, number4);
        int maxOfTwo = Math.max(max1, max2);
        System.out.println(maxOfTwo);
        // or in one statement System.out.println(Math.max(Math.max(number1, number2), Math.max(number3, number4)));

        System.out.print("\n====================The max of 3 numbers====================\n");
     int num5 = -30, num6 = -40, num7 = 0;
     int maxx = Math.max(num5, num6);
        System.out.println(Math.max(maxx, num7));

        System.out.print("\n====================The max of 5 numbers====================\n");
        int a = 5, b = 10, c = 50, d = 189, f = 12;
//Math.max(Math.max(a,b), Math.max(c,d) - the max of the first 2 number sets
        System.out.println(Math.max(Math.max(Math.max(a,b), Math.max(c,d)), f ));// just add one in front with parenthesis









    }
}
