package arrays;

public class NullKeyword {
    public static void main(String[] args) {

        String s1 = null;
        String s2 = "Jehad";
        System.out.println(s1);
        System.out.println(s2);

        //System.out.println(s1.length());//runtime error
        System.out.println(s2.length());//5
    }
}
