package arrays;

import java.util.Arrays;

public class Exercise03_SearchInArray {
    public static void main(String[] args) {
        String[] objects = {"Remote", "Mouse", "Mouse", "Keyboard", "iPad"};

/*
Check the collection you have above and print true if it contains Mouse
Print false otherwise

RESULT:
true
*/


        Arrays.sort(objects);//[Keyboard, Mouse, Mouse, Remote, iPad]
        if (Arrays.binarySearch(objects, "Mouse") >= 0) System.out.println(true);

    }
}
