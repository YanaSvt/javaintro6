package arrays;

import java.util.Arrays;

public class SearchingInAnArray {
    public static void main(String[] args) {
        int[] numbers = {3, 10 ,8 ,5, 5};

        // Check if this array has an element equals 7, print true if 7 exists, and false otherwise -> false

        //not the best way
   /*     int count7 = 0;

        for (int number : numbers) {
            if(number == 7) count7++;
        }

        if(count7 == 0) System.out.println(false);
        else System.out.println(true)
        boolean if7;
        for(int number : numbers){
            if ()System.out.println(number);
        }*/

        System.out.print("\n====================Loop way====================\n");
        boolean has7 = false;
        for (int number : numbers) {
            if(number == 7) {
                has7 = true;
                break;
            }
        }
        System.out.println(has7); // false

        System.out.print("\n====================Binary Search way====================\n");
        Arrays.sort(numbers);//binary search can not be used without sorting
        System.out.println(Arrays.toString(numbers));

        System.out.println(Arrays.binarySearch(numbers, 10));//4
        System.out.println(Arrays.binarySearch(numbers, 3));//0
        System.out.println(Arrays.binarySearch(numbers, 5));// returns index of 5 -> 2 last index of element

        System.out.println(Arrays.binarySearch(numbers, 7)); // -4 -> [3, 5, 5, 7, 8, 10] -3-1 -> -4
        System.out.println(Arrays.binarySearch(numbers, 15)); // -6 -> [3, 5, 5, 8, 10, 15] -5-1 -> -6
        System.out.println(Arrays.binarySearch(numbers, 1)); // -1 [1, 3, 5, 5, 8, 10] -0-1 -> -1



    }
}
