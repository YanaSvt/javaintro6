package arrays;

import java.util.Arrays;

public class UnderstandingArrays {
    public static void main(String[] args) {
        String[] cities = {"Chicago", "Miami", "Toronto"};
        System.out.println(cities.length); // 3 elements
        System.out.println(cities[1]);
        System.out.println(cities[0]);
        System.out.println(cities[2]);

        //How to print the array -> all elements
        //1. Convert your array to a string
        //Print it
        System.out.println(Arrays.toString(cities)); // [Chicago, Miami, Toronto]

        //HOW TO LOOP AN ARRAY
        System.out.print("\n====================for loop====================\n");
        for (int i = 0; i < cities.length; i++) {
            System.out.println(cities[i]);
        }

        System.out.print("\n=========for loop each loop - enhanced loop==========\n");//always increasing. Preferred when work with collections
        for(String varName : cities){
            System.out.println(varName);//city can be varName here
        }

        int[] numbers = new int[5];
        System.out.println(Arrays.toString(numbers));//[0, 0, 0, 0, 0]

    }
}
