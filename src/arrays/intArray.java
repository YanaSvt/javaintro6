package arrays;

import java.util.Arrays;

public class intArray {
    public static void main(String[] args) {
        //Create an int array that will store 6 numbers
        int[] numbers = new int[6];
        //Print the array
        System.out.println(Arrays.toString(numbers));//[0, 0, 0, 0, 0, 0]
        System.out.println(numbers);// [I@1b6d3586 this gives a location
        numbers[0] = 5;
        numbers[2] = 15;
        numbers[4] = 25;
        System.out.println(Arrays.toString(numbers));//[5, 0, 15, 0, 25, 0]
        numbers[0] = 7;
        System.out.println(Arrays.toString(numbers));

        //Print each element with for each loop
        for(int num : numbers){
            System.out.print(num + " ");
        }
        System.out.println();
        //Print each element with for loop
        for (int i = 0; i < numbers.length; i++) {   // do not give <= numbers.length it is more than given range
            System.out.print(numbers[i] + " ");
        }












    }
}
