package arrays.practice05;

public class Exercise02 {
    /*
    Write a program to find the longest and shortest strings
in an String array
Test data:
String[] words = {"red", "blue", "yellow", "white"};
Expected output:
The longest word is = yellow
The shortest word is = red
NOTE: Make your code dynamic that works for any
given String array.
     */
    public static void main(String[] args) {
        String[] words = {"red", "blue", "yellow", "white"};
        getShortestLongestWord(words);
    }

    public static void getShortestLongestWord(String[] words){

        String shortest = words[0];
        String longest = words[0];

        for (String s : words) {
            if(s.length() > longest.length()) longest = s;
            else if(s.length() < shortest.length()) shortest = s;
        }
        System.out.println("The longest word is = " + longest);
        System.out.println("The shortest word is = " + shortest);




    }

}
