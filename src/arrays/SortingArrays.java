package arrays;

import java.util.Arrays;

public class SortingArrays {
    public static void main(String[] args) {
        int[] numbers = {5, 3,1, 0};
        String[] words = {"Alex", "ali", "John", "James"};

        //Sort them - after this lane yu can not back to original order
        Arrays.sort(numbers);
        Arrays.sort(words);

        //Print them back after sorting
        //System.out.println(Arrays.toString(numbers));
       // System.out.println(Arrays.toString(words));


    }
}
