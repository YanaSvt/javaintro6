package escape_sequences;

public class Exercise03 {
    public static void main(String[] args) {
        System.out.println("\n----------------TASK-1----------------\n");
        System.out.println("Monday\\Tuesday\\Wednesday");

        System.out.println("\n----------------TASK-2----------------\n");
        System.out.println("Good \\\\\\ Morning");
    }
}
