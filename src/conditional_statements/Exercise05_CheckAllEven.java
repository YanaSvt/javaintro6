package conditional_statements;

import java.util.Scanner;

public class Exercise05_CheckAllEven {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("Please enter 3 numbers");
        int num1 = input.nextInt();
        int num2 = input.nextInt();
        int num3 = input.nextInt();

        if (num1 % 2 == 0 && num2 % 2 ==0 && num3 % 2 == 0){
            System.out.println("true");
        } else {
            System.out.println("false");
        }

    }
}
