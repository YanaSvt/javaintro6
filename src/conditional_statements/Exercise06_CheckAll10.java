package conditional_statements;

public class Exercise06_CheckAll10 {
    public static void main(String[] args) {
        int randomNumber1 = (int) (Math.random() * 2) + 10;
        int randomNumber2 = (int) (Math.random() * 2) + 10;
        System.out.println(randomNumber1);
        System.out.println(randomNumber2);

        System.out.println(randomNumber1 == 10 && randomNumber2 == 10);

    }
}
