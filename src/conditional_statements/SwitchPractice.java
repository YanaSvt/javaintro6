package conditional_statements;

import java.sql.SQLOutput;
import java.util.Scanner;

public class SwitchPractice {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

       /* System.out.println("Please enter a number from 1 to 12");
        int num = input.nextInt();
        input.nextLine();

       if(num ==1) System.out.println("Jan");
        else if (num ==2)System.out.println("Feb");
        else if (num ==3)System.out.println("March");
        else if (num ==4)System.out.println("April");
        else if (num ==5)System.out.println("May");
        else if (num ==6)System.out.println("June");
        else if (num ==7)System.out.println("July");
        else if (num ==8)System.out.println("Aug");
        else if (num ==9)System.out.println("Sept");
        else if (num ==10)System.out.println("Oct");
        else if (num ==11)System.out.println("Nov");
        else if (num ==12)System.out.println("Dec");
        else System.out.println("Please enter a number from 1 to 12");*/
/*
        switch(num){
            case 1 : {
                System.out.println("Jan");
                break;
            }
            case 2: {System.out.println("Feb");
                break;
            }
            case 3: {System.out.println("March");
                break;
            }
            case 4: {System.out.println("April");
                break;
            }
            case 5: {System.out.println("May");
                break;
            }
            case 6: {System.out.println("June");
                break;
            }
            case 7: {System.out.println("July");
                break;
            }
            case 8: {System.out.println("Aug");
                break;
            }
            case 9: {System.out.println("Sept");
                break;
            }
            case 10: {System.out.println("Oct");
                break;
            }
            case 11: {System.out.println("Nov");
                break;
            }
            case 12: {System.out.println("Dec");
                break;
            }
            default: {
                System.out.println("Not a number 1 to 12");
            }

        } */

        System.out.println("Please enter either a, b, or c");
        String letter = input.nextLine();

        switch (letter){
            case "a": {
                System.out.println("You entered a");
                break;
            }
            case "b": {
                System.out.println("You entered b");
                break;
            }
            case "c": {
                System.out.println("You entered c");
                break;
            }
            default:{
                System.out.println("This is not a,b or c");
                break;
            }



        }










    }
}
