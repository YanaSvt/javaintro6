package conditional_statements;

public class IfElseSyntax {
    public static void main(String[] args) {
        /*
If else statements are used to control a flow of the program based on the condition.
Conditions should always return true or false. (not necessary boolean itself.
If block can be used without an else block.
         */

        boolean condition = true;
        if (condition){
            //This is if block and it will execute if condition is true
            System.out.println("A");
    }
        else{
            //This is false block and it will execute if condition is false
            System.out.println("B");
        }

        System.out.println("End of the program");







    }
}
