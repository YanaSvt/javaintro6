package conditional_statements;

public class Practice02 {
    public static void main(String[] args) {
        int numb = (int)(Math.random() * 51);
        System.out.println(numb);

      //  if(numb >= 10 && numb <=25) System.out.println(true);
      //  else System.out.println(false);

      System.out.println(numb >= 10 && numb <=25);//the shortest answer

        System.out.println( (numb >= 10 && numb <=25) ? (true):(false));//turnery way

        System.out.print("\n====================TASK2====================\n");

        int randomNumber2 = (int)(Math.random() * 100 )+1;
        System.out.println(randomNumber2);

        if(randomNumber2 <= 25){
            System.out.println(randomNumber2 + " is in the 1st half");
            System.out.println(randomNumber2 + " is in the 1st quarter");
        } else if(randomNumber2 <= 50){
            System.out.println(randomNumber2 + " is in the 1st half");
            System.out.println(randomNumber2 + " is in the 2st quarter");
        }else if(randomNumber2 <= 75){
            System.out.println(randomNumber2 + " is in the 2st half");
            System.out.println(randomNumber2 + " is in the 3rd quarter");
        } else{
            System.out.println(randomNumber2 + " is in the 2st half");
            System.out.println(randomNumber2 + " is in the 4th quarter");
        }

        System.out.print("\n====================TASK3====================\n");
        /*
        Requirement:
            -Assume you are given a single character. (It will be hard-coded)
            -If given char is a letter, then print "Character is a letter"
            -If given char is a digit, then print "Character is a digit"
            USE ASCII TABLE for this task
            Test data:
            'v'
            Expected result:
            Character is a letter
            Test data:
            '5'
            Expected result:
            Character is a digit
         */
        char c = '7';

        if((c >= 65 && c <= 90) || (c >= 97 && c <= 122)) {
            System.out.println("Character is a letter");
        }else if(c >= 48 && c <= 57) System.out.println("Character is a digit");



    }
}
