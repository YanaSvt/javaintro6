package conditional_statements;

import java.util.Scanner;

public class Exercise02_MathScore {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        System.out.println("Hey David! Please enter your math grade?");
        int userInput = input.nextInt();

        if (userInput >= 60){ //you can put anything here that can be eventually true or false
            //This is if block and it will execute if condition is true
            System.out.println("Awesome! You have passed the math class!");
        }
        else{
            //This is false block and it will execute if condition is false
            System.out.println("Sorry! You failed!");
        }
       // System.out.println("End of the program");


/*
Multiple options:
3 options
A number can be positive, negative or neutral

if (number > 0){
//positive
}
else if (number < 0){
//negative
}
else{
//neutral
}

4 options quarters 0-25, 25-50, 50-75, 75-100
if(number <= 25){
//first
}
else if(number > 25 && number <=50) - this one should be only number <=50, because this condition was already checked in the first if
else if
else


 */

    }
}
