package conditional_statements;

import java.util.Scanner;

public class Exercise01_EvenOrOdd {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Please enter a number ");
        int userInput = input.nextInt();


        if (userInput % 2 == 0){ //you can put anything here that can be eventually true or false
            //This is if block and it will execute if condition is true
            System.out.println("The number you entered is even");
        }
        else{
            //This is false block and it will execute if condition is false
            System.out.println("The number you entered is odd");
        }
        System.out.println("End of the program");


    }
}
