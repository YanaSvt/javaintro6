package singleton;

public class Driver {
    //instance variable which is also Driver
    private static Driver driver; // singleton.Phone@29453f44

    private Driver(){
        // default constructor
    }

    // Create a method that instantiate a Driver object and return it
    public static Driver getDriver(){
        if(driver == null) driver = new Driver();
        return driver;
    }
}
