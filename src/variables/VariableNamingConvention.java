package variables;

public class VariableNamingConvention {
    public static void main(String[] args) {
        System.out.print("\n----------------Reassigning a value for variable---------------\n");
double d1 = 10;
        System.out.println(d1);//10.0

        d1 = 15.5;
        System.out.println(d1);//15.5

        /*
        Written rules:
        No spaces allowed in variable name. no special characters allowed except _ and $ and currency, can not start with a number
        Unwritten Rules:
        CamelCase, can not start with upperCase
         */


    }
}
