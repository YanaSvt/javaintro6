package variables;

public class UnderstandingVariables {
    public static void main(String[] args) {
        int age; //declaring the variable without a value
        age = 25;//initializing variable
        String name = "John";//declaring and initializing a variable
        System.out.println(age);
    }
}
