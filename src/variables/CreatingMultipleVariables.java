package variables;

public class CreatingMultipleVariables {
    public static void main(String[] args) {
        System.out.print("\n----------------Declaring multiple variables---------------\n");
        int age1, age2, age3;//declaring
        double d4, d5 = 50.5, d6;//2 declared, 1 declared and initialized
        System.out.println(d5);
        //2 different or same data types can not be in one lane, only if they are separated with semi colon, but this is not acceptable for reading


    }
}
