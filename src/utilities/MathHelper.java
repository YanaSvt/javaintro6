package utilities;

public class MathHelper {

    //method that returns the greatest number from 3
    public static int max(int n1, int n2, int n3) {
        return Math.max(Math.max(n1,n2),n3);

        }
    //Write a public static method named sum() and returns the sum of 2 numbers
    public static int sum(int s1,  int s2) {
        return s1 + s2;
    }
//Method Overloading (same name, but different data types).
   public static double sum(double sum1, double sum2, double sum3){
   return sum1 + sum2 + sum3;
   }
public int caughtSpeeding(int speed, boolean isBirthday) {
        if (isBirthday) {
            if (speed <= 60) return 0 + 5;
            else if (speed >= 61 && speed <= 80) return 1 + 5;
            else return 2 + 5;
        } else {
            if (speed <= 60) return 0;
            else if (speed >= 61 && speed <= 80) return 1;
            else return 2;

        }
/*

        //Write a public static method named product() and returns the product of 2 numbers
        public static int product ( int pr1, int pr2){
            return pr1 * pr2;
        }
        //Write a public static method named square() and returns the square of a number
        public static double square ( double sq1){
            return Math.sqrt(sq1);
      / }

*/
   }

}
