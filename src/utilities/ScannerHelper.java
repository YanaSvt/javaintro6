package utilities;

import java.util.Scanner;

public class ScannerHelper {
    static Scanner input = new Scanner(System.in);

    //write a method to get first name for user
    public static String getFirstName(){
        System.out.println("Please enter a First Name");
        return input.nextLine();
    }
    //write a method to get a Last name for user
    public static String getLastName(){
        System.out.println("Please enter a Last Name");
        return input.nextLine();
    }

    //write a method to get an age for user
    public static int getAge(){
        System.out.println("Please enter an age");
        int age = input.nextInt();
        input.nextLine();
        return age;
    }

    //write a method to get a number for user
    public static int getNumber(){
        System.out.println("Please enter a number");
        int number = input.nextInt();
        input.nextLine();
        return number;
    }
    //write a method to get a String
    public static String getString() {
        System.out.println("Please enter a string");
        String str = input.nextLine();
        return str;
    }
    //write a method to get a favorite book
    public static String getBook() {
        System.out.println("Please enter your favorite book");
        String book = input.nextLine();
        return book;
    }

    //write a method to get a full address
    public static String getAddress() {
        System.out.println("Please enter your full address");
        String adr = input.nextLine();
        return adr;
    }
    //write a method to get a full address
    public static String getfullName() {
        System.out.println("Please enter your full name");
        String fullName = input.nextLine();
        return fullName;
    }
    //write a method to get a Username
    public static String getUsername() {
        System.out.println("Please enter a username");
        String userName = input.nextLine();
        return userName;
    }
        //write a method to get a sentence
        public static String getSentence() {
            System.out.println("Please enter a sentence");
            String sentence = input.nextLine();
            return sentence;

        }




}
