package utilities;

public class Printer {
    //Write a public static method that prints good morning
    public static void  printGM(){
        System.out.println("Good Morning");
    }
    // Write a public static method that takes a name, and print it with Hello -> helloName
    public static void helloName(String name){
        System.out.println("Hello " + name);

    }

    public void printTechGlobal(){
        System.out.println("TechGlobal");
    }

    public static void combineString(String s1, String s2){
        System.out.println(s1 + s2);
    }


}
