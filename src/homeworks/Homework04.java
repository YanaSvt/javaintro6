package homeworks;

import utilities.ScannerHelper;

import java.util.Scanner;

public class Homework04 {
    public static void main(String[] args) {
       System.out.print("\n====================TASK1====================\n");
        String name = ScannerHelper.getFirstName();

        System.out.println("The length of the name is = " + name.length());
        System.out.println("The first character in the name is = " + name.charAt(0));
        System.out.println("The last character in the name is = " + name.charAt(name.length()-1));
        System.out.println("The first 3 characters in the name are = " + name.substring(0,3));//3 non inclusive
        System.out.println("The last 3 characters in the name are = " + name.substring(name.length()-3));

        //just wanted to try switch instead of if/else
        switch (name.charAt(0)) {
            case 'A':
                System.out.println("You are in the club!");
                break;
            case 'a':
                System.out.println("You are in the club!");
                break;
            default:
                System.out.println("Sorry, you are not in the club");
        }
        System.out.print("\n====================TASK2====================\n");
        String adr = ScannerHelper.getAddress();

        if (adr.toLowerCase().contains("chicago")) System.out.println("You are in the club");
        else if (adr.toLowerCase().contains("des plaines")) System.out.println("You are welcome to join to the club");
        else System.out.println("Sorry, you will never be in the club");

        System.out.print("\n====================TASK3====================\n");
        Scanner input = new Scanner(System.in);
        System.out.println("Please enter your favorite Country");
        String country = input.nextLine().toLowerCase();

        if (country.contains("a") && country.contains("i")) System.out.println("A and i are there");
        else if(country.contains("a")) System.out.println("A is there");
        else if (country.contains("i")) System.out.println("I is there");
        else System.out.println("A and i are not there");

        System.out.print("\n====================TASK4====================\n");
        String fun = "Java is FUN";

        String fW = fun.substring(0, 4).toLowerCase().trim();
        String sW = fun.substring(5, 7).toLowerCase().trim();
        String tW = fun.substring(8).toLowerCase().trim();


        System.out.println("The first word in the str is = " + fW);
        System.out.println("The second word in the str is = " + sW);
        System.out.println("The third word in the str is = " + tW);



    }
}
