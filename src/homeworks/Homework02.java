package homeworks;

import java.util.Scanner;

public class Homework02 {
    public static void main(String[] args) {

        System.out.print("\n====================TASK1====================\n");
        int input1, input2;

        Scanner input = new Scanner(System.in);
        System.out.println("Please enter a Number 1");
        input1 = input.nextInt();
        System.out.println("The number 1 entered by user is = " + input1 + "\n");


        System.out.println("Please enter a Number 2");
        input2 = input.nextInt();
        System.out.println("The number 2 entered by user is = " + input2 + "\n");


        System.out.println("The sum of number 1 and 2 entered by user is =  " + (input1 + input2));

        System.out.print("\n====================TASK2====================\n");
        int product1, product2;

        System.out.println("Please enter a Number 1");
        product1 = input.nextInt();
        System.out.println("Please enter a Number 2");
        product2 = input.nextInt();
        System.out.println("The product of the given 2 numbers is: " + (product1 * product2) );

        System.out.print("\n====================TASK3====================\n");
        double float1, float2;

        System.out.println("Please enter a Number 1");
        float1 = input.nextInt();
        System.out.println("Please enter a Number 2");
        float2 = input.nextInt();

        System.out.println("The sum of the given numbers is = " + (float1 + float2));
        System.out.println("The product of the given numbers is = " + float1 * float2);
        System.out.println("The subtraction of the given numbers is = " + (float1 - float2));
        System.out.println("The division of the given numbers is = " + float1 / float2);
        System.out.println("The remainder of the given numbers is = " + float1 % float2);

        System.out.print("\n====================TASK4====================\n");

        System.out.println("1.\t\t" + (-10 + 7 * 5));
        System.out.println("2.\t\t" + (72+24) % 24);
        System.out.println("3.\t\t" + (10 + -3*9 / 9));
        System.out.println("4.\t\t" + (5 + 18 / 3 * 3 - (6 % 3)));

        System.out.print("\n====================TASK5====================\n");
        int avg1, avg2;

        System.out.println("Please enter a Number 1");
        avg1 = input.nextInt();
        System.out.println("Please enter a Number 2");
        avg2 = input.nextInt();
        System.out.println("The average of the given numbers is: " + (avg1+avg2)/2 );

        System.out.print("\n====================TASK6====================\n");
        int average1, average2, average3, average4, average5;

        System.out.println("Please enter a Number 1");
        average1 = input.nextInt();
        System.out.println("Please enter a Number 2");
        average2 = input.nextInt();
        System.out.println("Please enter a Number 3");
        average3 = input.nextInt();
        System.out.println("Please enter a Number 4");
        average4 = input.nextInt();
        System.out.println("Please enter a Number 5");
        average5 = input.nextInt();

        System.out.println("The average of the given numbers is: " + (average1 + average2 + average3 + average4 + average5)/5 );

        System.out.print("\n====================TASK7====================\n");
        int sq1, sq2, sq3;

        System.out.println("Please enter a Number 1");
        sq1 = input.nextInt();
        System.out.println("Please enter a Number 2");
        sq2 = input.nextInt();
        System.out.println("Please enter a Number 3");
        sq3 = input.nextInt();

        System.out.println("The " + sq1 + " multiplied with " + sq1 + " is = " + sq1*sq1);
        System.out.println("The " + sq2 + " multiplied with " + sq2 + " is = " + sq2*sq2);
        System.out.println("The " + sq3 + " multiplied with " + sq3 + " is = " + sq3*sq3);

        System.out.print("\n====================TASK8====================\n");

        System.out.println("Please enter the side of a square");
        int square = input.nextInt();
        input.nextLine(); //to fix scanner bug

        System.out.println("Perimeter of the square = " + square * 4);
        System.out.println("Area of the square =  " + square * square);

        System.out.print("\n====================TASK9====================\n");
        double salary = 90000;
        System.out.println("A Software Engineer in Test can earn $" + salary * 3 + " in 3 years.");

        System.out.print("\n====================TASK10====================\n");

        System.out.println("Please enter your favorite book");
        String favBook = input.nextLine();

        System.out.println("Please enter your favorite color");
        String favColor = input.next();

        System.out.println("Please enter your favorite number");
        int favNumber = input.nextInt();
        input.nextLine(); //to fix scanner bug

        System.out.println("User's favorite book is: " + favBook + "\n User's favorite color is: " + favColor + "\n User's favorite number is: " + favNumber);

        System.out.print("\n====================TASK11====================\n");

        System.out.println("Please enter your First Name");
        String firstName = input.nextLine();

        System.out.println("Please enter your Last Name");
        String lastName = input.nextLine();

        System.out.println("Please enter your age");
        int age = input.nextInt();

        System.out.println("Please enter your email");
        String email = input.next();
        input.nextLine(); //to fix scanner bug

        System.out.println("Please enter your phone number");
        String phone = input.nextLine();

        System.out.println("Please enter your address");
        String address = input.nextLine();

        System.out.println("\tUser who joined this program is " + firstName + " " + lastName + ". " + firstName + "'s age is " + age + ". " + firstName + "'s email\n address is " + email + ", phone number is " + phone + ", and address\n is " + address + ".");



    }
}
