package homeworks;

import java.util.HashMap;

public class Homework16_lastInMock4 {
    //TASK01

    /**
     * Write a method called as parseData() which takes a String has some keys in {} and values after between
     * }{ and returns a collection that has all the keys and values as entries.
     * NOTE: The keys should be sorted!
     */
    public static HashMap<Integer,String> parseData(String str){
        HashMap<Integer, String> hashMap = new HashMap<>();
        String[] arr = str.split("[{}]");

        for (int i = 1; i < arr.length; i++) {
            hashMap.put(Integer.parseInt(arr[i]),arr[i+1]);
            i++;
        }
        return hashMap;
    }

    //TASK02
    public static double calculateTotalPrice1(HashMap<String, Integer> hm){
        double price = 0.0;

        for (String s : hm.keySet()) {
            switch (s){
                case "Apple":
                    price += hm.get("Apple") * 2.00;
                    break;
                case "Mango":
                    price += hm.get("Mango") * 4.99;
                    break;
                case "Orange":
                    price += hm.get("Orange") * 3.29;
                    break;
                case "Pineapple":
                    price += hm.get("Pineapple") * 5.25;
                    break;
            }
        }
        return price;

    }

    //TASK03
    public static double calculateTotalPrice2(HashMap<String, Integer> hm){
        double price = 0;

        for (String s : hm.keySet()) {
            switch (s){
                case "Apple":
                    if(hm.get("Apple") % 2 == 0) price += hm.get("Apple") * 1.50;
                    else price += (hm.get("Apple") * 1.50) + .50;
                    break;
                case "Mango":
                    int freeMango = hm.get("Mango")/4;
                    price += (hm.get("Mango") * 4.99) - (freeMango * 4.99);
                    break;
                case "Orange":
                    price += hm.get("Orange") * 3.29;
                    break;
            }
        }
        return price;
    }

    public static void main(String[] args) {
        System.out.print("\n====================TASK1====================\n");
        System.out.println(parseData("{104}LA{101}Paris{102}Berlin{103}Chicago{100}London"));

        System.out.print("\n====================TASK2====================\n");
        HashMap<String,Integer> hm = new HashMap<>();
        hm.put("Apple", 3);
        hm.put("Mango", 1);
        System.out.println(calculateTotalPrice1(hm));

        HashMap<String,Integer> hm2 = new HashMap<>();
        hm2.put("Apple", 2);
        hm2.put("Pineapple", 1);
        hm2.put("Orange", 3);
        System.out.println(calculateTotalPrice1(hm2));

        System.out.print("\n====================TASK3====================\n");
        HashMap<String,Integer> hm3 = new HashMap<>();
        hm3.put("Apple", 3);
        hm3.put("Mango", 5);
        System.out.println(calculateTotalPrice2(hm3));

        HashMap<String,Integer> hm4 = new HashMap<>();
        hm4.put("Apple", 4);
        hm4.put("Mango", 8);
        hm4.put("Orange", 3);
        System.out.println(calculateTotalPrice2(hm4));
    }
}
