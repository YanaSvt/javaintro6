package homeworks;

import java.util.Arrays;

public class Homework12 {

    //TASK01
    /*
    -Create a method called noDigit()
-This method will take one String argument and it will return a new String with all digits removed from the original String
     */
    public static String noDigit(String str){
        StringBuilder result = new StringBuilder();
        for (char c : str.toCharArray()) {
            if (!Character.isDigit(c)) {
                result.append(c);
            }
        }
        return result.toString();
    }

    //TASK02
    /*
    Create a method called noVowel()
-This method will take one String argument and it will return a new String with all vowels removed from the original String
     */
    public static String noVowel(String str){
        return str.replaceAll("[aeiouAEIOU]", "");
    }

    //TASK03
    /*
    Create a method called sumOfDigits()
-This method will take one String argument and it will return an int sum of all digits from the original String.
     */
    public static int sumOfDigits(String str){
        int sum = 0;
        for (char c : str.toCharArray()) {
            if (Character.isDigit(c)) {
                sum += Character.getNumericValue(c);
            }
        }
        return sum;
    }

    //TASK04
    /*
    Create a method called hasUpperCase()
-This method will take one String argument and it will return boolean true if there is an uppercase letter and false otherwise.
     */
    public static Boolean hasUpperCase(String str){
        //StringBuilder result = new StringBuilder();
        for (char c : str.toCharArray()) {
            if (Character.isUpperCase(c)) return true;
        }
        return false;
    }

    //TASK05
    /*
    Create a method called middleInt()
-This method will take three int arguments and it will return the middle int.
     */
    public static int middleInt(int a, int b, int c){
        int[] arr = {a, b, c};
        Arrays.sort(arr);
        return arr[1];
        }


    //TASK06
    /*
    Create a method called no13()
-This method will take an int array as argument and it will return a new array with all 13 replaced with 0.
     */
    public static int[] no13(int[] arr){
        int[] result = new int[arr.length];
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == 13) {
                result[i] = 0;
            } else {
                result[i] = arr[i];
            }
        }
        return result;
    }

    //TASK07
    /*
    Create a method called arrFactorial()
-This method will take an int array as argument and it will return the array with every number replaced with their factorials.
     */
    public static int[] arrFactorial(int[] nums){
        int[] result = new int[nums.length];
        if (nums.length == 0) {
            return result;  // Return empty array if input array is empty
        }

        for (int i = 0; i < nums.length; i++) {
            int num = nums[i];
            if (num == 0) {
                result[i] = 1;  // 0! equals to 1
            } else {
                int fact = 1;
                for (int j = 1; j <= num; j++) {
                    fact *= j;
                }
                result[i] = fact;
            }
        }

        return result;
    }

    //TASK08
    /*
    Create a method called categorizeCharacters()
-This method will take String as an argument and return a String array as letters at index of 0, digits at index of 1 and specials at index of 2.
NOTE: IGNORE SPACES
NOTE: Assume length will always be more than zero.

     */
    public static String[] categorizeCharacters(String str){
        return new String[]{str.replaceAll("[^a-zA-Z]", ""),
                str.replaceAll("[^0-9]", ""),
                str.replaceAll("[a-zA-Z0-9 ]", "")};
    }



    public static void main(String[] args) {
        System.out.print("\n====================TASK1====================\n");
        System.out.println(noDigit(""));
        System.out.println(noDigit("Java"));
        System.out.println(noDigit("123Hello"));
        System.out.println(noDigit("123Hello World149"));
        System.out.println(noDigit("123Tech456Global149"));

        System.out.print("\n====================TASK2====================\n");
        System.out.println(noVowel(""));
        System.out.println(noVowel("xyz"));
        System.out.println(noVowel("JAVA"));
        System.out.println(noVowel("125$"));
        System.out.println(noVowel("TechGlobal"));

        System.out.print("\n====================TASK3====================\n");
        System.out.println(sumOfDigits(""));
        System.out.println(sumOfDigits("Java"));
        System.out.println(sumOfDigits("John’s age is 29"));
        System.out.println(sumOfDigits("$125.0"));

        System.out.print("\n====================TASK4====================\n");
        System.out.println(hasUpperCase(""));
        System.out.println(hasUpperCase("java"));
        System.out.println(hasUpperCase("John’s age is 29"));
        System.out.println(hasUpperCase("$125.0"));

        System.out.print("\n====================TASK5====================\n");
        System.out.println(middleInt(1,1,1));
        System.out.println(middleInt(1,2,2));
        System.out.println(middleInt(5,5,8));
        System.out.println(middleInt(5,3,5));
        System.out.println(middleInt(-1,25,10));

        System.out.print("\n====================TASK6====================\n");
        System.out.println(Arrays.toString(no13(new int[]{1, 2, 3 ,4})));
        System.out.println(Arrays.toString(no13(new int[]{13, 2, 3 })));
        System.out.println(Arrays.toString(no13(new int[]{13, 13, 13 , 13, 13})));

        System.out.print("\n====================TASK7====================\n");
        System.out.println(Arrays.toString(arrFactorial(new int[]{1, 2, 3 ,4})));
        System.out.println(Arrays.toString(arrFactorial(new int[]{0,5})));
        System.out.println(Arrays.toString(arrFactorial(new int[]{5,0,6})));
        System.out.println(Arrays.toString(arrFactorial(new int[]{})));

        System.out.print("\n====================TASK8====================\n");
        System.out.println(Arrays.toString(categorizeCharacters("     ")));
        System.out.println(Arrays.toString(categorizeCharacters("abc123$#%")));
        System.out.println(Arrays.toString(categorizeCharacters("12ab$%3c%")));



    }

}
