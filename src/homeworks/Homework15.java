package homeworks;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;

public class Homework15 {

    //TASK01
    /**
     * Create a method called fibonacciSeries1()
     * -This method will take an int argument as n, and it will return n series of Fibonacci numbers as an int array.
     * REMEMBER: Fibonacci series = 0, 1, 1, 2, 3, 5, 8, 13, 21
     */
    public static int[] fibonacciSeries1(int n) {
        int[] result = new int[n];
        for (int i = 0; i < n; i++) {
            if (i == 0) result[i] = 0;
            else if (i == 1) result[i] = 1;
            else result[i] = result[i - 1] + result[i - 2];
        }
        return result;
    }

    //TASK02
    /**
     * Create a method called fibonacciSeries2()
     * -This method will take an int argument as n, and it will return the nth series of Fibonacci number as an int.
     * REMEMBER: Fibonacci series = 0, 1, 1, 2, 3, 5, 8, 13, 21
     */
    public static int fibonacciSeries2(int n) {
        if (n <= 1) return 0;
        if (n <= 3) return 1;
        return fibonacciSeries2(n - 1) + fibonacciSeries2(n - 2);
    }

    //TASK03
    /**
     *Create a method called findUniques()
     * -This method will take 2 int array argument and it will return an int array which has only the unique values from both given arrays.
     * NOTE: If both arrays are empty, then return an empty array.
     * NOTE: if one of the array is empty, then return unique values from the other array.
     */
    public static int[] findUniques(int[] arr1, int[] arr2){
        ArrayList<Integer> al = new ArrayList<>();
        HashSet<Integer> set1 = new HashSet<>();
        HashSet<Integer> set2 = new HashSet<>();

        for (int i : arr1) {
            set1.add(i);
        }

        for (int i : arr2) {
            set2.add(i);
        }

        for (Integer i : set1) {
            if(!set2.contains(i) && !al.contains(i)) al.add(i);
        }
        for (Integer i : set2) {
            if(!set1.contains(i) && !al.contains(i)) al.add(i);
        }

        int[] answer = new int[al.size()];
        for (int i = 0; i < answer.length; i++) {
            answer[i] = al.get(i);
        }
        return answer;
    }

    //TASK04
    /**
     * Create a method called firstDuplicate()
     * -This method will take an int array argument and it will return an int which is the first duplicated number.
     * NOTE: All elements will be positive numbers.
     * NOTE: If there are no duplicates, then return -1
     * NOTE: If there are more than one duplicate, then return the one for which second occurrence has the smallest index.
     */
    public static int firstDuplicate(int[] arr){
        int dup = -1;
        int index = arr.length-1;

        for (int i = 0; i < arr.length-1; i++) {
            for (int j = i+1; j < arr.length; j++) {
                if(arr[i] == arr[j] && j<=index){
                    dup = arr[i];
                    index = j;
                }
            }
        }
        return dup;
    }

    //TASK05
    /**
     *Create a method called isPowerOf3()
     * -This method will take an int argument and it will return true if given int argument is equal to 3 power of the X. Otherwise, it will return false.
     * Numbers that are power of 3 = 1, 3, 9, 27, 81, 243….
     */
    public static boolean isPowerOf3(int n){
        int power = 0;
        for (int i = 0; n > power; i++) {
            power = (int)Math.pow(3,i);
            if(power == n) return true;
        }
        return false;
    }

        public static void main (String[]args){
            System.out.print("\n====================TASK1====================\n");
            System.out.println(Arrays.toString(fibonacciSeries1(3)));
            System.out.println(Arrays.toString(fibonacciSeries1(5)));
            System.out.println(Arrays.toString(fibonacciSeries1(7)));

            System.out.print("\n====================TASK2====================\n");
            System.out.println(fibonacciSeries2(2));
            System.out.println(fibonacciSeries2(4));
            System.out.println(fibonacciSeries2(8));

            System.out.print("\n====================TASK3====================\n");
            System.out.println(Arrays.toString(findUniques(new int[]{}, new int[]{})));
            System.out.println(Arrays.toString(findUniques(new int[]{}, new int[]{1, 2, 3, 2})));
            System.out.println(Arrays.toString(findUniques(new int[]{1, 2, 3, 4}, new int[]{3, 4, 5, 5})));
            System.out.println(Arrays.toString(findUniques(new int[]{8, 9}, new int[]{9, 8, 9})));

            System.out.print("\n====================TASK4====================\n");
            System.out.println(firstDuplicate(new int[]{}));
            System.out.println(firstDuplicate(new int[]{1}));
            System.out.println(firstDuplicate(new int[]{1, 2, 2, 3}));
            System.out.println(firstDuplicate(new int[]{1,  2, 3, 3, 4, 1}));

            System.out.print("\n====================TASK5====================\n");
            System.out.println(isPowerOf3(1));
            System.out.println(isPowerOf3(2));
            System.out.println(isPowerOf3(3));
            System.out.println(isPowerOf3(81));


        }

    }
