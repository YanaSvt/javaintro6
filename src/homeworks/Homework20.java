package homeworks;

import java.util.Arrays;

public class Homework20 {
    //TASK01

    /**
     * Write a method called as sum() that takes an int array and a boolean  and returns the either the count of even or odd numbers based on boolean value.
     * NOTE: if the boolean value is true, the method should return the count of the even elements.
     * If the boolean value is false, the method should return the count of the odd elements.
     */
    public static int sum(int[] numbers, boolean isEvenCount) {
        int count = 0;
        for (int number : numbers) {
            if (isEvenCount && number % 2 == 0) { // Count even numbers
                count++;
            } else if (!isEvenCount && number % 2 != 0) { // Count odd numbers
                count++;
            }
        }
        return count;
    }

    //TASK02

    /**
     * Write a method called as sumDigitsDouble() that takes a String and returns the sum of the digits in the given String multiplied by.
     * NOTE: your method should return -1 if the given String does not have any digits. Ignore negative numbers.
     */
    public static int sumDigitsDouble(String input) {
        int sum = 0;
        boolean hasDigits = false;

        for (char c : input.toCharArray()) {
            if (Character.isDigit(c)) {
                int digit = Character.getNumericValue(c);
                if (digit >= 0) {
                    sum += digit;
                    hasDigits = true;
                }
            }
        }

        return hasDigits ? sum * 2 : -1;
    }

    //TASK03

    /**
     * Write a method countOccurrence() that takes 2 String arguments and returns how many times that first String can form the second String.
     * NOTE: This method is case-insensitive and should ignore the white spaces!
     * Also ignore the position of the characters.
     */
    public static int countOccurrence(String firstString, String secondString) {
        // Remove whitespace and convert to lowercase
        firstString = firstString.replaceAll("\\s", "").toLowerCase();
        secondString = secondString.replaceAll("\\s", "").toLowerCase();

        int count = 0;
        int index = 0;

        while ((index = firstString.indexOf(secondString, index)) != -1) {
            count++;
            index++;
        }

        return count;
    }



    public static void main(String[] args) {
        System.out.print("\n====================TASK1====================\n");
        /*int[] numbers = {1, 5,10};
        boolean isEvenCount = true;
        int result = sum(numbers, isEvenCount);
        System.out.println(result);*/

        int[] numbers = {3, 7, 2, 5, 10};
        boolean isEvenCount = false;
        int result = sum(numbers, isEvenCount);
        System.out.println(result);

        System.out.print("\n====================TASK2====================\n");
        System.out.println(sumDigitsDouble("Java"));
        System.out.println(sumDigitsDouble("ab12"));
        System.out.println(sumDigitsDouble("23abc45"));
        System.out.println(sumDigitsDouble("Hi-23"));

        System.out.print("\n====================TASK3====================\n");
        System.out.println(countOccurrence("Hello", "World"));
        System.out.println(countOccurrence("Hello", "l"));
        System.out.println(countOccurrence("Can I can a can", "anc"));
        System.out.println(countOccurrence("IT conversations", "IT"));


    }
}
