package homeworks;

import java.util.Arrays;
import java.util.Calendar;

public class Homework11 {
    //TASK01
    /*
    -Create a method called noSpace()
-This method will take one String argument and it will return a new String with all spaces removed from the original String
     */
    public static String noSpace(String str){
        return str.trim().replaceAll("[\\s]+", "");
    }

    //TASK02
    /*
    -Create a method called replaceFirstLast()
-This method will take one String argument and it will return a new String with first and last characters replaced
NOTE: if the length is less than 2, then return empty String
NOTE: Ignore all before and after spaces (get actual String only)
     */
    public static String replaceFirstLast(String str){
        if (str.trim().length() < 2) return "";
        else return (str.charAt(str.length()-1) + str.substring(1,str.length()-1) + str.charAt(0));
    }

    //TASK03
    /*
    -Create a method called hasVowel()
-This method will take one String argument and it will return a boolean checking if String has any vowel or not
     */
    public static boolean hasVowel(String str){
        boolean hasVowels = false;
        for (int i = 0; i < str.length(); i++) {
            if (str.toLowerCase().contains("a") || str.toLowerCase().contains("e")
                    || str.toLowerCase().contains("o") || str.toLowerCase().contains("u") || str.toLowerCase().contains("i")) hasVowels = true;
        }
        return hasVowels;
    }

    //TASK04
    /*
    -Create a method called checkAge()
-This method will take an int yearOfBirth as  argument and it will print message below based on the entry
If the age is less than 16, then print “AGE IS NOT ALLOWED”
If the age is 16 or more, then print “AGE IS ALLOWED”
If the age is more than 100 or a future year entered, print “AGE IS NOT VALID”
NOTE: Calculate the age taking base year as current year in a dynamic way. You can use Date class.
     */
    public static void checkAge(int yearOfBirth){
        Calendar now = Calendar.getInstance();
        int currentYear = now.get(Calendar.YEAR);
        int age = currentYear - yearOfBirth;

        if (age < 0 || age > 100) {
            System.out.println("AGE IS NOT VALID");
        } else if (age < 16) {
            System.out.println("AGE IS NOT ALLOWED");
        } else {
            System.out.println("AGE IS ALLOWED");
        }
    }

    //TASK05
    /*
    -Create a method called averageOfEdges()
-This method will take three int arguments and it will return average of min and max numbers
     */
    public static int averageOfEdges(int a, int b, int c){
        int max = Math.max(Math.max(a,b),c);
        int min = Math.min(Math.min(a,b),c);
        return (max + min) / 2;
    }

    //TASK06
    /*
    -Create a method called noA()
-This method will take a String array argument and it will return a new array with all elements starting with A or a replaced with “###”
NOTE: Assume length will always be more than zero
NOTE: Ignore cases

     */
    public static String[] noA(String[] arr){
        String[] newArr = new String[arr.length];
        for (int i = 0; i < arr.length; i++) {
            if (arr[i].toLowerCase().startsWith("a")) newArr[i] = "###";
            else newArr[i] = arr[i];
        }
        return newArr;
    }

    //TASK07
    /*
    -Create a method called no3or5()
-This method will take an int array argument and it will return a new array with some elements replaced as below
If element can be divided by 5, replace it with 99
If element can be divided by 3, replace it with 100
If element can be divided by both 3 and 5, replace it with 101
NOTE: Assume length will always be more than zero
     */
    public static int[] no3or5(int[] arr){
        int[] newArr = new int[arr.length];
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] % 15 == 0) newArr[i] = 101;
            else if (arr[i] % 5 == 0) newArr[i] = 99;
            else if (arr[i] % 3 == 0) newArr[i] = 100;
            else newArr[i] = arr[i];
        }
        return newArr;
    }

    //TASK08
    /*
    -Create a method called countPrimes()
-This method will take an int array argument and it will return how many elements in the array are prime numbers
NOTE: Prime number is a number that can be divided only by 1 and itself
NOTE: Negative numbers cannot be prime
Examples: 2,3,5,7,11,13,17,19,23,29,31,37 etc.
NOTE: Smallest prime number is 2
     */

    public static int countPrimes(int[] arr){
        int count = 0;
        for (int num : arr) {
            if (num <= 1) continue;
            boolean isPrime = true;
            for (int i = 2; i < num; i++) {
                if (num % i == 0) {
                    isPrime = false;
                    break;
                }
            }
            if (isPrime) {
                count++;
            }
        }
        return count;
    }





    public static void main(String[] args) {

        System.out.print("\n====================TASK1====================\n");
        System.out.println(noSpace(""));
        System.out.println(noSpace("Java"));
        System.out.println(noSpace("    Hello    "));
        System.out.println(noSpace("Hello World   "));
        System.out.println(noSpace("Tech Global"));

        System.out.print("\n====================TASK2====================\n");
        System.out.println(replaceFirstLast(""));
        System.out.println(replaceFirstLast("A"));
        System.out.println(replaceFirstLast("    A       "));
        System.out.println(replaceFirstLast("Hello"));
        System.out.println(replaceFirstLast("Tech Global"));

        System.out.print("\n====================TASK3====================\n");
        System.out.println(hasVowel(""));
        System.out.println(hasVowel("Java"));
        System.out.println(hasVowel("1234"));
        System.out.println(hasVowel("ABC"));

        System.out.print("\n====================TASK4====================\n");
        checkAge(2010);
        checkAge(2006);
        checkAge(2050);
        checkAge(1920);
        checkAge(1800);

        System.out.print("\n====================TASK5====================\n");
        System.out.println(averageOfEdges(0, 0 ,0));
        System.out.println(averageOfEdges(0, 0 ,6));
        System.out.println(averageOfEdges(-2, -2 ,10));
        System.out.println(averageOfEdges(-3, 15 ,-3));
        System.out.println(averageOfEdges(10, 13 ,20));

        System.out.print("\n====================TASK6====================\n");
        System.out.println(Arrays.toString(noA(new String[] {"java", "hello", "123", "xyz"})));
        System.out.println(Arrays.toString(noA(new String[] {"appium", "123", "ABC", "java"})));
        System.out.println(Arrays.toString(noA(new String[] {"apple", "appium", "ABC", "Alex", "A"})));

        System.out.print("\n====================TASK7====================\n");
        System.out.println(Arrays.toString(no3or5((new int[] {7, 4, 11, 23, 17}))));
        System.out.println(Arrays.toString(no3or5((new int[] {3, 4, 5, 6}))));
        System.out.println(Arrays.toString(no3or5((new int[] {10, 11, 12, 13, 14, 15}))));

        System.out.print("\n====================TASK8====================\n");
        System.out.println(countPrimes((new int[] {-10, -3, 0, 1})));
        System.out.println(countPrimes((new int[] {7, 4, 11, 23, 17})));
        System.out.println(countPrimes((new int[] {41, 53, 19, 47, 67})));


    }

}


































































































































































































































































































































































































































































































































































































































































