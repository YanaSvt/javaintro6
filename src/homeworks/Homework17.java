package homeworks;

public class Homework17 {
    //TASK01

    /**
     * Write a method called as nthWord() that takes a String and an int arguments and returns the nth word in the String.
     * NOTE: your method should return empty string if int argument is greater than the count of the words in the String.
     */
    public static String nthWord(String input, int n) {
        String[] words = input.split("\\s+");

        if (n <= 0 || n > words.length) {
            return "";
        }
        return words[n - 1];
    }

    //TASK02

    /**
     * Write a method called as isArmStrong() that takes an int argument and returns true if given number is arm-strong, and false otherwise.
     * NOTE: An Armstrong number is one whose sum of digits raised to the power three equals the number itself.
     * Let's take an example to understand it better. Consider the number 153.
     * To determine if 153 is an Armstrong number, we need to check if the sum of its digits, each raised to the power of the number of digits, equals the original number.
     * 1^3 + 5^3 + 3^3 = 1 + 125 + 27 = 153
     * In this case, the sum of the individual digits raised to the power of 3 (the number of digits in 153) is equal to the original number, which means 153 is an Armstrong number.
     */
    public static boolean isArmstrong(int num) {
        int originalNum = num;
        int sum = 0;

        // Calculate the sum of digits raised to the power of three
        while (num > 0) {
            int digit = num % 10;
            sum += Math.pow(digit, 3);
            num /= 10;
        }

        // Check if the sum is equal to the original number
        return sum == originalNum;
    }

    //TASK03

    /**
     * Write a method reverseNumber() that takes an int argument and returns it back reversed without converting it to a String.
     */
    public static int reverseNumber(int num) {
        int reversedNum = 0;

        while (num != 0) {
            int digit = num % 10;
            reversedNum = reversedNum * 10 + digit;
            num /= 10;
        }

        return reversedNum;
    }

    public static void main(String[] args) {
        System.out.print("\n====================TASK1====================\n");
        System.out.println(nthWord(("I like programming languages"), 2));
        System.out.println(nthWord(("QA stands for Quality Assurance"), 4));
        System.out.println(nthWord(("Hello World"), 3));

        System.out.print("\n====================TASK2====================\n");
        System.out.println(isArmstrong((153)));
        System.out.println(isArmstrong((123)));

        System.out.print("\n====================TASK3====================\n");
        System.out.println(reverseNumber((371)));
        System.out.println(reverseNumber((12)));

    }
}
