package homeworks;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Homework10 {
    //TASK01
    /*
    Write a method countWords() that takes a String as an argument, and returns how many words there are in the the given String
     */
    public static int countWords(String str) {
       /* Pattern pattern = Pattern.compile("[a-zA-Z]{1,}");
        Matcher matcher = pattern.matcher(str);
        int wordCount = 0;

        while(matcher.find()){
            wordCount++;
        }
        return wordCount;
    }*/
        return str.trim().split("[\\s]+").length;
    }

    //TASK02
    /*
    Write a method countA() that takes a String as an argument, and returns how many A or a there are in the the given String
     */
    public static int countA(String str){
        int counter = 0;
        for (char c : str.toCharArray()) {
            if (Character.toLowerCase(c) == 'a') counter++;
        }
        return counter;
    }
    /*
    return str.replaceAll("[^aA]", "").length();
     */

    //TASK03
    /*
    Write a method countPos() that takes an ArrayList of Integer as an argument, and returns how many elements are positive
     */
    public static int countPos(ArrayList<Integer> list){
        int count = 0;
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i) > 0) count++;
        }
        return count;
    }
    //return (int) list.stream().filter(e -> e > 0).count();

    //TASK04
    /*
    Write a method removeDuplicateNumbers() that takes an ArrayList of Integer as an argument, and returns it back with removed duplicates
     */
    public static ArrayList<Integer> removeDuplicateNumbers(ArrayList<Integer> list){
        for (int i = 0; i < list.size()-1; i++) {
            for (int j = i+1; j < list.size(); j++) {
                if (list.get(i) == list.get(j))list.remove(j);
            }
        }
        return list;
    }
    /*
    ArrayList<Integer> newList = new ArrayList<>();
        for (int s : list) {
            if(!newList.contains(s)) newList.add(s);
        }
        return newList;
     */

    //TASK05
    /*
    Write a method removeDuplicateElements() that takes an ArrayList of String as an argument, and returns it back with removed duplicates
     */
    public static ArrayList<String> removeDuplicateElements(ArrayList<String> list) {
        for (int i = 0; i < list.size()-1; i++) {
            for (int j = i+1; j < list.size(); j++) {
                if (list.get(i).equals(list.get(j)))list.remove(j);
            }
        }
        return list;
    }
    /*
    for (String s : list) {
            if(!newList.contains(s)) newList.add(s);
        }
        return newList;
     */

    //TASK06
    /*
    Write a method removeExtraSpaces() that takes a String as an argument, and returns a String with removed extra spaces
     */
    public static String removeExtraSpaces(String str){
        return str.trim().replaceAll("[\\s]+", " ");
    }
    /*
    String newStr = "";
//
//        char[] strAsArr = str.toCharArray();
//
//        for (int i = 0; i < strAsArr.length; i++) {
//            if(strAsArr[i] != ' ') newStr += strAsArr[i];
//            else if(i != strAsArr.length-1 && strAsArr[i+1] != ' ') newStr += strAsArr[i];
//        }
//
//        return newStr.trim();
     */

    //TASK07
    /*
    Write a method add() that takes 2 int[] arrays as arguments and returns a new array with sum of given arrays elements.
     */
    public static int[] add(int[] arr1, int[] arr2) {
        int maxLength = Math.max(arr1.length, arr2.length);
        int[] result = new int[maxLength];

        for (int i = 0; i < maxLength; i++) {
            int num1 = (i < arr1.length) ? arr1[i] : 0;
            int num2 = (i < arr2.length) ? arr2[i] : 0;
            result[i] = num1 + num2;
        }
        return result;
    }
    /*
    for (int i = 0; i < Math.min(arr1.length, arr2.length); i++) {
            if(arr1.length> arr2.length) arr1[i] += arr2[i];
            else arr2[i] += arr1[i];
        }

        return arr1.length > arr2.length ? arr1 : arr2;
     */

    //TASK08
    /*
    Write a method findClosestTo10() that takes an int[] array as an argument, and returns the closest element to 10 from given array
     */
    public static int findClosestTo10(int[] arr){
        int closest = Integer.MAX_VALUE;
        for (int i = 1; i < arr.length; i++) {
            if (arr[i] < 10 && Math.abs(arr[i] - 10) < Math.abs(closest - 10)) {
                closest = arr[i];
            }
        }
        return closest;
    }
    /*
    Arrays.sort(arr);
        int closest = Integer.MAX_VALUE;
        for (int number : arr) {
            if (number != 10 && Math.abs(number - 10) < Math.abs(closest - 10)) {
                closest = number;
            }
        }
        return closest;
     */

    public static void main(String[] args) {
        System.out.print("\n====================TASK1====================\n");
        System.out.println(countWords("      Java is fun       "));
        System.out.println(countWords("Selenium is the most common UI automation tool.  "));
        //System.out.println(countWords(""));

        System.out.print("\n====================TASK2====================\n");
        System.out.println(countA("TechGlobal is a QA bootcamp"));
        System.out.println(countA("QA stands for Quality Assurance"));
        //System.out.println(countA(""));

        System.out.print("\n====================TASK3====================\n");
        System.out.println(countPos(new ArrayList<>(Arrays.asList(-45, 0, 0, 34, 5, 67))));
        System.out.println(countPos(new ArrayList<>(Arrays.asList(-23, -4, 0, 2, 5, 90, 123))));

        System.out.print("\n====================TASK4====================\n");
        System.out.println(removeDuplicateNumbers(new ArrayList<>(Arrays.asList(10, 20, 35, 20, 35, 60, 70, 60))));
        System.out.println(removeDuplicateNumbers(new ArrayList<>(Arrays.asList(1, 2, 5, 2, 3))));

        System.out.print("\n====================TASK5====================\n");
        System.out.println(removeDuplicateElements(new ArrayList<>(Arrays.asList("java", "C#", "ruby", "JAVA", "ruby", "C#", "C++"))));
        System.out.println(removeDuplicateElements(new ArrayList<>(Arrays.asList("abc", "xyz", "123", "ab", "abc", "ABC"))));

        System.out.print("\n====================TASK6====================\n");
        System.out.println(removeExtraSpaces("   I   am      learning     Java      "));
        System.out.println(removeExtraSpaces("Java  is fun    "));

        System.out.print("\n====================TASK7====================\n");
        System.out.println(Arrays.toString(add(new int[]{3, 0, 0, 7, 5, 10}, new int[]{6, 3, 2})));
        System.out.println(Arrays.toString(add(new int[]{10, 3, 6, 3, 2}, new int[]{6, 8, 3, 0, 0, 7, 5, 10, 34})));

        System.out.print("\n====================TASK8====================\n");
        System.out.println(findClosestTo10(new int[]{10, -13, 5, 70, 15, 57}));
        System.out.println(findClosestTo10(new int[]{10, -13, 8, 12, 15, -20}));

    }
}