package homeworks;

import java.util.Arrays;

public class Homework06 {
    public static void main(String[] args) {
        System.out.print("\n====================TASK1====================\n");
        int[] numbers = new int[10];

        numbers[2] = 23;
        numbers[4] = 12;
        numbers[7] = 34;
        numbers[9] = 7;
        numbers[6] = 15;
        numbers[0] = 89;

        System.out.println(numbers[3]);
        System.out.println(numbers[0]);
        System.out.println(numbers[9]);
        System.out.println(Arrays.toString(numbers));

        System.out.print("\n====================TASK2====================\n");
        String[] str = new String[5];

        str[1] = "abc";
        str[4] = "xyz";

        System.out.println(str[3]);
        System.out.println(str[0]);
        System.out.println(str[4]);
        System.out.println(Arrays.toString(str));

        System.out.print("\n====================TASK3====================\n");
        int[] nums = {23, -34, -56, 0, 89, 100};
        System.out.println(Arrays.toString(nums));
        Arrays.sort(nums);
        System.out.println(Arrays.toString(nums));

        System.out.print("\n====================TASK4====================\n");
        String[] countries = {"Germany", "Argentina", "Ukraine", "Romania"};
        System.out.println(Arrays.toString(countries));
        Arrays.sort(countries);
        System.out.println(Arrays.toString(countries));

        System.out.print("\n====================TASK5====================\n");
        String[] dogs = {"Scooby Doo", "Snoopy", "Blue", "Pluto", "Dino", "Sparky"};
        System.out.println(Arrays.toString(dogs));
        Arrays.sort(dogs);
        if(Arrays.binarySearch(dogs, "Pluto") >=0) System.out.println(true);

        System.out.print("\n====================TASK6====================\n");
        String[] cats = {"Garfield", "Tom", "Sylvester", "Azrael"};
        Arrays.sort(cats);
        System.out.println(Arrays.toString(cats));

        System.out.println(Arrays.binarySearch(cats, "Garfield") >= 0 && Arrays.binarySearch(cats, "Felix") >= 0);

        System.out.print("\n====================TASK7====================\n");
        double[] doubles = {10.5, 20.75, 70, 80, 15.75};
        System.out.println(Arrays.toString(doubles));
        System.out.println(doubles[0]);
        System.out.println(doubles[1]);
        System.out.println(doubles[2]);
        System.out.println(doubles[3]);
        System.out.println(doubles[4]);

        System.out.print("\n====================TASK8====================\n");
        char[] characters = {'A', 'b', 'G', 'H', '7', '5', '&', '*', 'e', '@', '4'};
        System.out.println(Arrays.toString(characters));

        int letters = 0;
        int lowercase = 0;
        int uppercase= 0;
        int digits = 0;
        int specials = 0;
        for(int type : characters){
            if (type >= 65 && type <= 90 || type >= 97 && type <= 122) {
                letters++;
                if (type >= 97 && type <= 122) lowercase++;
                else uppercase++;
            }
            else if (type >= 48 && type <= 57) digits++;
            else specials++;
        }
        System.out.println("Letters = " + letters);
        System.out.println("Uppercase letters = " + uppercase);
        System.out.println("Lowercase letters = " + lowercase);
        System.out.println("Digits = " + digits);
        System.out.println("Special characters = " + specials);

        System.out.print("\n====================TASK9====================\n");
        String[] objects = {"Pen", "notebook", "Book", "paper", "bag", "pencil", "Ruler"};
        System.out.println(Arrays.toString(objects));

        int startsWithUpper = 0;
        int startsWithLower = 0;
        int startsWithBorP = 0;
        int havingBookorPen = 0;

        for (String count : objects) {
            if (count.charAt(0) >= 65 && count.charAt(0) <= 90) startsWithUpper++;
            else if (count.charAt(0) >= 97 && count.charAt(0) <= 122) startsWithLower++;
        }
        for (String contains : objects) {
            if (contains.toLowerCase().startsWith("b") || contains.toLowerCase().startsWith("p")) startsWithBorP++;
        }
        for (String bookPen : objects) {
            if (bookPen.toLowerCase().contains("book") || bookPen.toLowerCase().contains("pen")) havingBookorPen++;
        }
        System.out.println("Elements starts with uppercase = " + startsWithUpper);
        System.out.println("Elements starts with lowercase = " + startsWithLower);
        System.out.println("Elements starts with B or P = " + startsWithBorP);
        System.out.println("Elements having \"book\" or \"pen\" = " + havingBookorPen);

        System.out.print("\n====================TASK10====================\n");
        int[] wholeNumbers = {3, 5,  7, 10, 0, 20, 17, 10, 23, 56, 78};
        System.out.println(Arrays.toString(wholeNumbers));

        int moreThan10 = 0;
        int lessThan10 = 0;
        int is10 = 0;
        for (int wholeNumber : wholeNumbers){
            if (wholeNumber > 10) moreThan10++;
            else if (wholeNumber < 10) lessThan10++;
            else if (wholeNumber == 10) is10++;
        }
        System.out.println("Elements that are more than 10 = " + moreThan10);
        System.out.println("Elements that are less than 10 = " + lessThan10);
        System.out.println("Elements that are 10 = " + is10);

        System.out.print("\n====================TASK11====================\n");
        int[] arr1 = {5, 8, 13, 1, 2};
        int[] arr2 = {9, 3, 67, 1, 0};
        int[] arr3 = new int[5];
        System.out.println("1st array is = " + Arrays.toString(arr1));
        System.out.println("2nd array is = " + Arrays.toString(arr2));

        for (int i = 0; i < 5; i++) {
            arr3[i] = Math.max(arr1[i],arr2[i]);
        }
        System.out.println("3rd array is = " + Arrays.toString(arr3));

































    }
}
