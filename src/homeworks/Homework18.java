package homeworks;

import java.util.Arrays;

public class Homework18 {

    //TASK01
    /**
     * Write a method called as doubleOrTriple() that takes an int array and a boolean  and returns the array elements either doubled or tripled based on boolean value.
     * NOTE: if the boolean value is true, the elements in the array should be doubled. If the boolean value is false, the elements should be tripled.
     */
    public static int[] doubleOrTriple(int[] arr, boolean doubleValues) {
        int[] result = new int[arr.length];
        int multiplier = doubleValues ? 2 : 3;

        for (int i = 0; i < arr.length; i++) {
            result[i] = arr[i] * multiplier;
        }

        return result;
    }

    //TASK02
    /**
     * Write a method called as splitString() that takes a String and an int arguments and returns the String back split by the given int.
     * NOTE: your method should return empty String if the length of given String cannot be divided to given number.
     */
    public static String splitString(String input, int splitSize) {
        int length = input.length();
        if (length % splitSize == 0) {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < length; i += splitSize) {
                sb.append(input.substring(i, i + splitSize)).append(" ");
            }
            return sb.toString().trim();
        } else {
            return "";
        }
    }

    //TASK03
    /**
     * Write a method countPalindrome() that takes String and returns how many words in the String are palindrome words.
     * NOTE: A palindrome word is a word that reads the same forwards and backwards. Example: level, radar, deed, refer…
     * NOTE: This method is case-insensitive!
     */
    public static int countPalindrome(String input) {
        String[] words = input.split(" ");
        int count = 0;
        for (String word : words) {
            String lowercaseWord = word.toLowerCase();
            int left = 0;
            int right = lowercaseWord.length() - 1;
            boolean isPalindrome = true;
            while (left < right) {
                if (lowercaseWord.charAt(left) != lowercaseWord.charAt(right)) {
                    isPalindrome = false;
                    break;
                }
                left++;
                right--;
            }
            if (isPalindrome) {
                count++;
            }
        }
        return count;
    }


    public static void main(String[] args) {
        System.out.print("\n====================TASK1====================\n");
        System.out.println(Arrays.toString(doubleOrTriple(new int[] {1,5,10}, true)));
        System.out.println(Arrays.toString(doubleOrTriple(new int[] {3,7,2}, false)));

        System.out.print("\n====================TASK2====================\n");
        System.out.println(splitString("Java", 2));
        System.out.println(splitString("JavaScript", 5));
        System.out.println(splitString("Hello", 3));

        System.out.print("\n====================TASK3====================\n");
        System.out.println(countPalindrome("Mom and Dad"));
        System.out.println(countPalindrome("Kayak races attracts racecar drivers"));

    }
}
