package homeworks;

public class Homework01 {
    public static void main(String[] args) {
        System.out.print("\n----------------TASK1---------------\n");
/*
JAVA = 01001010 01000001 01010110 01000001
SELENIUM = 01010011 01000101 01001100 01000101 01001110 01001001 01010101 01111101
 */
        System.out.print("\n----------------TASK2---------------\n");
        /*
01001101 - 77 - M
01101000 - 104 - h
01111001 - 121 - y
01010011 - 83 - S
01101100 - 108 - l
*/
        System.out.print("\n----------------TASK3---------------\n");

        System.out.println("I start to practice \"JAVA\" today, and I like it.");
        System.out.println("The secret of getting ahead is getting started.");
        System.out.println("\"Don't limit yourself.\"");
        System.out.println("Invest in your dreams. Grind now. Shine later.");
        System.out.println("It’s not the load that breaks you down, it’s the way you carry it.");
        System.out.println("The hard days are what make you stronger.");
        System.out.println("You can waste your lives drawing lines. Or you can live your life crossing them.");

        System.out.print("\n----------------TASK4---------------\n");

        System.out.println("\tJava is easy to write and easy to run—this is the foundational\nstrength of Java and why many developers program in it. When you\nwrite Java once, you can run it almost anywhere at any time.\n\n\tJava can be used to create complete applications that can run on\na single computer or be distributed across servers and clients in a\n network.\n\n\tAs a result, you can use it to easily build mobile applications or\nrun-on desktop applications that use different operating systems and\nservers, such as Linux or Windows.");

        System.out.print("\n----------------TASK5---------------\n");
        byte myAge = 36;
        short myFavoriteNumber = 130;
        double myHeight = 5.06;
        byte myWeight = 118;
        char myFavoriteLetter = 'Y';

        System.out.println("My age is " + myAge + " years old");
        System.out.println("My favorite number is " + myFavoriteNumber);
        System.out.println("My Height is " + myHeight);
        System.out.println("My Weight is " + myWeight + " lb");
        System.out.println("My Favorite Letter is " + "\"" + myFavoriteLetter + "\"");

    }
}
