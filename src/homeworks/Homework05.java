package homeworks;

import utilities.ScannerHelper;

import java.util.Scanner;

public class Homework05 {
    public static void main(String[] args) {
        System.out.print("\n====================TASK1====================\n");
/*
Write a program that prints all the numbers that are dividable by 7 from 1 to 100 (1 and 100 are included). Result in one lane with dashes
 */
        String solution = "";
        for (int i = 1; i <= 100 ; i++) {
            if(i % 7 == 0) solution += i + " - ";
        }
        System.out.println(solution.substring(0,solution.length() - 3));


       /* //while loop
        int i = 1;
        while(i <= 100){
            if (i == 98) System.out.print(i);
            else if (i % 7 == 0 )System.out.print( i + " - ");
            i++;
        }

        //do while loop
        int i = 1;
        do{
            if (i == 98) System.out.print(i);
            else if (i % 7 == 0 )System.out.print( i + " - ");
            i++;
        }
        while(i <= 100);*/

        System.out.print("\n====================TASK2====================\n");
        solution = "";
        for (int i = 1; i <= 50 ; i++) {
            if(i % 6 == 0) solution += i + " - ";
        }
        System.out.println(solution.substring(0,solution.length() - 3));
       /* for (int m = 1; m <= 50 ; m++) {
            if (m == 48) System.out.print(m);
            else if (m % 6 == 0) System.out.print(m + " - ");
        }

        //while loop
        int m = 1;
        while(m <= 50){
            if (m == 48) System.out.print(m);
            else if (m % 6 == 0 )System.out.print( m + " - ");
            m++;
        }

        //do while loop
        int m = 1;
        do{
            if (m == 48) System.out.print(m);
            else if (m % 6 == 0 )System.out.print( m + " - ");
            m++;
        }
        while(m <= 50);*/

        System.out.print("\n====================TASK3====================\n");
        solution = "";
        for (int i = 100; i >= 50 ; i--) {
            if(i % 5 == 0) solution += i + " - ";
        }
        System.out.println(solution.substring(0,solution.length() - 3));

       /* for (int b = 100; b >= 50; b--) {
            if (b == 50) System.out.print(b);
            else if (b % 5 == 0) System.out.print(b + " - ");
        }

      /*  //while loop
        int b = 100;
        while(b >= 50){
            if (b == 50) System.out.print(b);
            else if (b % 5 == 0) System.out.print(b + " - ");
            b--;
        }

        //do while loop
        int b = 100;
        do{
            if (b == 50) System.out.print(b);
            else if (b % 5 == 0) System.out.print(b + " - ");
            b--;
        }
        while(b >= 50);*/

        System.out.print("\n====================TASK4====================\n");

        for (int c = 0; c <= 7; c++) {
            System.out.println("The square of " + c + " is = " + c*c);
        }

       /* //while loop
        int c = 0;
        while(c <= 7){
            System.out.println("The square of " + c + " is = " + c*c);
            c++;
        }

        //do while loop
        int c = 0;
        do{
            System.out.println("The square of " + c + " is = " + c*c);
            c++;
        }
        while(c <= 7);*/

        System.out.print("\n====================TASK5====================\n");

        int sum = 0;
        for (int d = 1; d <= 10; d++) {
            sum += d;
        }
        System.out.println(sum);

       /*//while loop
        int sum = 0;
        int d = 1;
        while(d <= 10){
            sum += d;
            d++;
        }
        System.out.println(sum);

       //do while loop
        int sum = 0;
        int d = 1;
        do{
            sum += d;
            d++;
        }
        while(d <= 10);
        System.out.println(sum);*/

        System.out.print("\n====================TASK6====================\n");

        Scanner input = new Scanner(System.in);
        System.out.println("Please enter a positive number");
        int userInput = input.nextInt();

        int fact = 1;
        for (int f = 1; f <= userInput; f++) {
            fact *= f;
        }
        System.out.println(fact);

       /*  //while loop
        int fact = 1;
        int f = 1;
        while(f <= userInput){
            fact *= f;
            f++;
        }
        System.out.println(fact);

       //do while loop
        int fact = 1;
        int f = 1;
        do{
            fact *= f;
            f++;
        }
        while(f <= userInput);
        System.out.println(fact); */

        System.out.print("\n====================TASK7====================\n");

        String str = ScannerHelper.getfullName();
        int count = 0;

        for (int g = 0; g <= str.length()-1; g++) {
            if(str.toLowerCase().charAt(g) == 'a' || str.toLowerCase().charAt(g) == 'e' || str.toLowerCase().charAt(g) == 'i' || str.toLowerCase().charAt(g) == 'o' || str.toLowerCase().charAt(g) == 'u') count++;
        }
        if (count == 1) System.out.println("There is " + count + " vowel letter in this full name");
        else System.out.println("There are " + count + " vowel letters in this full name");

        /*//while loop
        int g = 0;
        while(g <= str.length()-1){
            if(str.toLowerCase().charAt(g) == 'a' || str.toLowerCase().charAt(g) == 'e' || str.toLowerCase().charAt(g) == 'i' || str.toLowerCase().charAt(g) == 'o' || str.toLowerCase().charAt(g) == 'u') count++;
            g++;
        }
        if (count == 1) System.out.println("There is " + count + " vowel letter in this full name");
        else System.out.println("There are " + count + " vowel letters in this full name");

       //do while loop
        int g = 0;
        do{
            if(str.toLowerCase().charAt(g) == 'a' || str.toLowerCase().charAt(g) == 'e' || str.toLowerCase().charAt(g) == 'i' || str.toLowerCase().charAt(g) == 'o' || str.toLowerCase().charAt(g) == 'u') count++;
            g++;
        }
        while(g <= str.length()-1);
        if (count == 1) System.out.println("There is " + count + " vowel letter in this full name");
        else System.out.println("There are " + count + " vowel letters in this full name");*/

        System.out.print("\n====================TASK8====================\n");

        String name;
        do{
            name = ScannerHelper.getFirstName();
        }while(!name.toLowerCase().startsWith("j"));
        System.out.println("End of the program");

        /*String name;
        Scanner scanner = new Scanner(System.in);
        //do while loop
        do{
            System.out.println("Please enter a name");
            name = scanner.nextLine();
        }
        while(name.toLowerCase().charAt(0) != 'j');
        System.out.println("End of the program");*/



    }
}