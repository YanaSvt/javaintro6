package homeworks;

import java.util.*;

public class Homework09 {
    public static void main(String[] args) {
        System.out.print("\n====================TASK1====================\n");
        //Test 1
        int[] nums1 = {-4, 0, -7, 0, 5, 10, 45, 45};
        int firstDuplicate1 = findFirstDuplicate(nums1);
        if (firstDuplicate1 != -1) {
            System.out.println(firstDuplicate1);
        } else {
            System.out.println("There is no duplicates");
        }

        //Test 2
        int[] nums2 = {-8, 56, 7, 8, 65};
        int firstDuplicate2 = findFirstDuplicate(nums2);
        if (firstDuplicate2 != -1) {
            System.out.println(firstDuplicate2);
        } else {
            System.out.println("There is no duplicates");
        }

        //Test 3
        int[] nums3 = {3, 4, 3, 3, 5, 5, 6, 6, 7};
        int firstDuplicate3 = findFirstDuplicate(nums3);
        if (firstDuplicate3 != -1) {
            System.out.println(firstDuplicate3);
        } else {
            System.out.println("There is no duplicates");
        }

        System.out.print("\n====================TASK2====================\n");
        System.out.println(findDuplicateString(new String[]{"Z", "abc", "z", "123", "#"}));
        System.out.println(findDuplicateString(new String[]{"xyz", "java", "abc"}));
        System.out.println(findDuplicateString(new String[]{"a", "b", "B", "XYZ", "123"}));

        System.out.print("\n====================TASK3====================\n");
        System.out.println(duplicatedNumbers(new int[]{0, -4, -7, 0, 5, 10, 45, -7, 0}));
        System.out.println(duplicatedNumbers(new int[]{1, 2, 5, 0, 7}));

        System.out.print("\n====================TASK4====================\n");
        System.out.println(duplicatedStrings(new String[]{"A", "foo", "12", "Foo", "bar", "a", "a", "java"}));
        System.out.println(duplicatedStrings(new String[]{"python", "foo", "bar", "java", "123"}));

        System.out.print("\n====================TASK5====================\n");
        System.out.println(Arrays.toString(reversedArray(new String[]{"abc", "foo", "bar"})));
        System.out.println(Arrays.toString(reversedArray(new String[]{"java", "python", "ruby"})));

        System.out.print("\n====================TASK6====================\n");
        System.out.println(reverseStringWords("Java is fun"));
        System.out.println(reverseStringWords("Today is a fun day"));
    }

    //TASK01
    /*
    Write a method called as firstDuplicatedNumber to find the first duplicated number in an int array.
NOTE: Make your code dynamic that works for any given int array and return -1 if there are no duplicates in the array.

     */
    public static int findFirstDuplicate(int[] nums) {
        for (int i = 0; i < nums.length; i++) {
            for (int j = i + 1; j < nums.length; j++) {
                if (nums[i] == nums[j]) {
                    return nums[i];
                }
            }
        }
        return -1; // If no duplicates found
    }

    /*
public static int firstDuplicatedNumber(int[] arr){
        ArrayList<Integer> container = new ArrayList<>();

        for (int i : arr) {
            if(container.contains(i)){
                return i;
            }else  container.add(i);
        }
        return -1;
    }
     */

    //TASK02
    /*
    Write a method called as firstDuplicatedString to find the first duplicated String in a String array, ignore cases.
NOTE: Make your code dynamic that works for any given String array and return “There is no duplicates” if there are no duplicates in the array.
     */
    public static String findDuplicateString(String[] arr) {

        for (int i = 0; i < arr.length; i++) {
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[i].equalsIgnoreCase(arr[j])) return arr[i];
            }
        }
        return "There is no duplicates";
    }
    /*
    public static String firstDuplicatedString (String[] arr){
        ArrayList<String> container = new ArrayList<>();

        for (String s : arr) {
            if(container.contains(s.toLowerCase())){
                return s;
            }else  container.add(s.toLowerCase());
        }
        return "There is no duplicates";
    }
     */

    //TASK03
    /*
    Write a method called as duplicatedNumbers to find the all duplicates in an int array.
NOTE: Make your code dynamic that works for any given int array and return empty ArrayList if there are no duplicates in the array.
     */
    public static ArrayList<Integer> duplicatedNumbers(int[] arr) {
        ArrayList<Integer> duplicates = new ArrayList<>();

        for (int i = 0; i < arr.length - 1; i++) {
            for (int j = i + 1; j < arr.length; j++) {
                if (!duplicates.contains(arr[i]) && arr[i] == arr[j]) duplicates.add(arr[i]);
            }
        }
        return duplicates;
    }
    /*
 public static ArrayList<Integer> duplicatedStrings(int[] arr){
        ArrayList<Integer> dupContainer = new ArrayList<>();

        for (int i = 0; i < arr.length - 1; i++) {
            for (int j = i + 1; j < arr.length; j++) {
                if(arr[i] == arr[j] && !dupContainer.contains(arr[i])) dupContainer.add(arr[i]);
            }
        }
        return dupContainer;
    }
     */

    //TASK04
    /*
    Write a method called as duplicatedStrings to find the all duplicates in a String array, ignore cases.
NOTE: Make your code dynamic that works for any given String array and return and empty ArrayList if there are no duplicates in the array.
     */
    public static ArrayList<String> duplicatedStrings(String[] arr) {
        ArrayList<String> duplicates = new ArrayList<>();

        for (int i = 0; i < arr.length; i++) {
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[i].equalsIgnoreCase(arr[j]) && !duplicates.contains(arr[i]))
                    duplicates.add(arr[i]);//I gave up here:)

            }
        }
        return duplicates;
    }
    /*
    public static ArrayList<String> duplicatedStrings(String[] arr){
        ArrayList<String> dupContainer = new ArrayList<>();
        ArrayList<String> solution = new ArrayList<>();

        for (int i = 0; i < arr.length - 1; i++) {
            for (int j = i + 1; j < arr.length; j++) {
                if(arr[i].equalsIgnoreCase(arr[j]) && !dupContainer.contains(arr[i].toLowerCase())){

                    dupContainer.add(arr[i].toLowerCase());
                    solution.add(arr[i]);
                }
            }
        }
        return solution;
    }
     */


    //TASK05
    /*
    Write a method called as reversedArray to reverse elements in an array, then print array.
NOTE: Make your code dynamic that works for any given array.

     */
    public static String[] reversedArray(String[] arr) {
        StringBuilder reversed = new StringBuilder();

        for (int i = arr.length; i > 0; i--) {
            reversed.append(arr[i - 1]).append(" ");
        }
        String[] reversedArray = reversed.toString().split(" ");
        return reversedArray;
    }
   /*
   WAY: 1 -> Max Iteration
        ArrayList<String> reversedList = new ArrayList<>();
        for (int i = arr.length-1; i >= 0 ; i--) {
            reversedList.add(arr[i]);
        }
        System.out.println(reversedList);



        Second way:
        arr = a, b, c, d, e, f
        i: 0
        startingIndex: 0 1 2 3
        endingIndex: 5 4 3 2
        Storage: a, b, c
        first i: f, b, c, d, e, a
        sec i:   f, e, c, d, b, a
        third i: f, e, d, c, b, a
        int startingIndex = 0;
        int endingIndex = arr.length-1;
        String storage;
        while(startingIndex < endingIndex){
            storage = arr[startingIndex];
            arr[startingIndex] = arr[endingIndex];
            arr[endingIndex] = storage;
            startingIndex++;
            endingIndex--;
        }
        System.out.println(Arrays.toString(arr));


    //Way 3:
        Collections.reverse(Arrays.asList(arr));
        System.out.println(Arrays.toString(arr));
    */

    //TASK06
    /*
    Write a method called as reverseStringWords to reverse each word in a given String
NOTE: Make your code dynamic that works for any given String.
     */
    public static String reverseStringWords(String str) {
        String reveredStr = "";
        String[] strAsArr = str.trim().split("\\s+");
        for (String s : strAsArr) {
            reveredStr += new StringBuffer(s).reverse() + " ";
        }
        return reveredStr.trim();
    }
}