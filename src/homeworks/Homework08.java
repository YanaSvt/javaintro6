package homeworks;

import utilities.ScannerHelper;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Homework08 {
    public static void main(String[] args) {
        System.out.print("\n====================TASK1====================\n");
        System.out.println(countConsonants("hello"));
        System.out.println(countConsonants("JAVA"));
        System.out.println(countConsonants(""));

        System.out.print("\n====================TASK2====================\n");
        System.out.println(Arrays.toString(wordArray("hello")));
        System.out.println(Arrays.toString(wordArray("java  is    fun")));
        System.out.println(Arrays.toString(wordArray("Hello, nice to meet you!!")));

        System.out.print("\n====================TASK3====================\n");
        System.out.println(removeExtraSpaces("hello"));
        System.out.println(removeExtraSpaces("java  is    fun"));
        System.out.println(removeExtraSpaces("Hello,    nice to   meet     you!!"));

       System.out.print("\n====================TASK4====================\n");
       System.out.println(count3OrLess(ScannerHelper.getSentence()));

        System.out.print("\n====================TASK5====================\n");
        System.out.println(isDateFormatValid("01/21/1999"));
        System.out.println(isDateFormatValid("1/20/1991"));
        System.out.println(isDateFormatValid("10/2/1991"));
        System.out.println(isDateFormatValid("12-20 2000"));
        System.out.println(isDateFormatValid("12/16/19500"));

        System.out.print("\n====================TASK6====================\n");
        System.out.println(isEmailFormatValid("abc@gmail.com"));
        System.out.println(isEmailFormatValid("abc@student.techglobal.com"));
        System.out.println(isEmailFormatValid("a@gmail.com"));
        System.out.println(isEmailFormatValid("abcd@@gmail.com"));
        System.out.println(isEmailFormatValid("abc@gmail"));

    }
    //TASK01
    /*
Write a method named countConsonants() that takes a String as an argument a returns how many consonants are in the String.
 */
    public static int countConsonants(String str){
        str = str.replaceAll("[aeiouAEIOU]", "");//removing all vowels from the string
        return str.length();
    }

    //TASK02
    /*
    Write a method named wordArray() that takes a String as an argument a returns a String array with all the words in the String
     */
    public static String[] wordArray(String str){
        str = str.replaceAll("[^a-zA-Z0-9]", " ");
        String strArray[] = str.split("\\s+");
        return strArray;
    }

    //TASK03
    /*
    Write a method named removeExtraSpaces() that takes a String as an argument a returns the String back with all extra spaces removed.
     */
    public static String removeExtraSpaces(String str){
        return str.replaceAll("\\s+", " ");
    }

    //TASK04
    /*
    Write a method named count3OrLess() that asks the user to enter a sentence. Return a count of how many words are 3 characters long or less.
     */
   public static int count3OrLess(String str){
       Pattern pattern = Pattern.compile("\\b\\w{1,3}\\b");
       Matcher matcher = pattern.matcher(str);
        int wordCount = 0;

       while(matcher.find()){
           matcher.group();
           wordCount++;
       }
       return wordCount;
  }

  //TASK05
    /*
    Write a method named isDateFormatValid() that takes a String dateOfBirth as an argument and checks if the given date matches the given DOB requirements.
This method would return a true or false boolean
Format: nn/nn/nnnn
     */
    public static boolean isDateFormatValid(String dob){

        return Pattern.matches("^[0-3][0-9]/[0-3][0-9]/(?:[0-9][0-9])?[0-9][0-9]$", dob);
    }

    //TASK06
    /*
Write a method named isEmailFormatValid() that takes a String email as an argument and checks if the given email matches the given email requirements.
This method would return a true or false boolean.
Format: <2+chars>@<2+chars>.<2+chars>
     */
    public static boolean isEmailFormatValid(String email){

        return Pattern.matches("[a-z0-9]{2,}+@[a-z]{2,}+\\.[a-z]{2,}+(\\.[a-z]{2,})?", email);

    }





}
