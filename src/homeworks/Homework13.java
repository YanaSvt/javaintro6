package homeworks;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;

public class Homework13 {
    //TASK01

    /**
     *-Create a method called hasLowerCase()
     * -This method will take a String argument, and it will return boolean true if there is lowercase letter and false if it doesn’t.
     */
    public static Boolean hasLowerCase(String str) {
        for (int i = 0; i < str.length(); i++) {
            if (Character.isLowerCase(str.charAt(i))) {
                return true;
            }
        }
        return false;
    }

    //TASK02

    /**
     Create a method called noZero()
     -This method will take one Integer ArrayList argument and it will return an ArrayList with all zeros removed from the original Integer ArrayList.
     NOTE: Assume that ArrayList size will be at least 1.
     */
    public static ArrayList<Integer> noZero(ArrayList<Integer> inputList) {
        ArrayList<Integer> resultList = new ArrayList<>();

        for (Integer num : inputList) {
            if (num != 0) {
                resultList.add(num);
            }
        }
        return resultList;
    }

    //TASK03

    /**
     Create a method called numberAndSquare()
     -This method will take an int array argument and it will return a multidimensional array with all numbers squared.
     NOTE: Assume that array size is at least 1.
     */
    public static int[][] numberAndSquare(int[] numbers) {
        int[][] answer = new int[numbers.length][2];//[{1,1},{2, 4},{3,9}]

        int index = 0;
        for (int i : numbers) {
            answer[index][0] = i;
            answer[index][1] = i * i;
            index++;
        }
        return answer;
    }
    //TASK04

    /**
     -Create a method called containsValue()
     -This method will take a String array and a String argument, and it will return a boolean.
     Search the variable inside of the array and return true if it exists in the array. If it doesn’t exist, return false.
     NOTE: Assume that array size is at least 1.
     NOTE: The method is case-sensitive
     */
    public static boolean containsValue(String[] array, String value) {
        int index = Arrays.binarySearch(array, value);
        return index >= 0;
    }

    //TASK05

    /**
     Create a method called reverseSentence()
     -This method will take a String argument and it will return a String with changing the place of every word.
     All words should be in reverse order. Make sure that there are two words inside the sentence at least.
     If there is no two words return “There is not enough words!”.
     NOTE: After you reverse, only first letter must be uppercase and the rest will be lowercase!
     Hint: Use split() for easy solution
     Note: Make the new first word’s first letter upper case and make the old first word’s first letter lower case
     */
    public static String reverseSentence(String str){
        str = str.toLowerCase();
        String[] strArr = str.trim().split("[ ]+");// [java, is, fun]
        String reveredStr = "";

        if(strArr.length < 2){
            return "There is not enough words!";
        }else{
            for (int i = strArr.length - 1; i >= 0 ; i--) {
                reveredStr += strArr[i] + " ";
            }
            reveredStr = reveredStr.trim();
            return reveredStr.substring(0,1).toUpperCase() + reveredStr.substring(1);
        }
    }

    //TASK06

    /**
     * Create a method called removeStringSpecialsDigits()
     * -This method will take a String as argument, and it will return a String without the special characters or digits.
     * NOTE: Assume that String length is at least 1.
     * NOTE: Do not remove spaces.
     */
    public static String removeStringSpecialsDigits(String input) {
        // Regular expression pattern to match special characters and digits
        String pattern = "[^a-zA-Z\\s]";

        // Remove special characters and digits using the pattern
        String result = input.replaceAll(pattern, "");

        return result;
    }

    //TASK07

    /**
     * Create a method called removeArraySpecialsDigits()
     * -This method will take a String array as argument, and it will return a String array without the special characters or digits from the elements.
     * NOTE: Assume that array size is at least 1.
     */
    public static String[] removeArraySpecialsDigits(String[] arr) {
        for (int i = 0; i < arr.length; i++) {
            arr[i] = arr[i].replaceAll("[^A-Za-z]", "");
        }
        return arr;
    }

    //TASK08

    /**
     Create a method called removeAndReturnCommons()
     -This method will take two String ArrayList and it will return all the common words as String ArrayList.
     NOTE: Assume that both ArrayList sizes are at least 1.
     */
    public static ArrayList<String> removeAndReturnCommons(ArrayList<String> list1, ArrayList<String> list2) {
        LinkedHashSet<String> set1 = new LinkedHashSet<>(list1);

        ArrayList<String> answer = new ArrayList<>();
        for (String s : set1) {
            if(list2.contains(s))
                answer.add(s);
        }
        return answer;
    }

    //TASK09

    /**
     * Create a method called noXInVariables()
     * -This method will take an ArrayList argument, and it will return an ArrayList with all the x or X removed from elements.
     * If the element itself equals to x or X or contains only x letters, then remove that element.
     * NOTE: Assume that ArrayList size is at least 1.
     */
    public static ArrayList<String> noXInVariables(ArrayList<String> arr) {
        for (int i = 0; i < arr.size(); i++) {
            arr.set(i, arr.get(i).replaceAll("[xX]", ""));
            if (arr.get(i).isEmpty()) {
                arr.remove(i);
                i--;
            }
        }
        return arr;
    }

    public static void main(String[] args) {
        System.out.print("\n====================TASK1====================\n");
        System.out.println(hasLowerCase(""));
        System.out.println(hasLowerCase("JAVA"));
        System.out.println(hasLowerCase("125$"));
        System.out.println(hasLowerCase("hello"));

        System.out.print("\n====================TASK2====================\n");
        System.out.println(noZero(new ArrayList<>(Arrays.asList(1))));
        System.out.println(noZero(new ArrayList<>(Arrays.asList(1, 1, 10))));
        System.out.println(noZero(new ArrayList<>(Arrays.asList(0, 1, 10))));
        System.out.println(noZero(new ArrayList<>(Arrays.asList(0, 0, 0))));

        System.out.print("\n====================TASK3====================\n");
        int[] numbers = {1, 2, 3};
        int[][] squaredNumbers = numberAndSquare(numbers);
        for (int i = 0; i < squaredNumbers.length; i++) {
            System.out.print("[" + squaredNumbers[i][0] + ", " + squaredNumbers[i][1] + "]");
        }
        System.out.println("");

        int[] numbers2 = {0, 3, 6};
        int[][] squaredNumbers2 = numberAndSquare(numbers2);
        for (int i = 0; i < squaredNumbers2.length; i++) {
            System.out.print("[" + squaredNumbers2[i][0] + ", " + squaredNumbers2[i][1] + "]");
        }
        System.out.println("");

        int[] numbers3 = {1, 4};
        int[][] squaredNumbers3 = numberAndSquare(numbers3);
        for (int i = 0; i < squaredNumbers3.length; i++) {
            System.out.print("[" + squaredNumbers3[i][0] + ", " + squaredNumbers3[i][1] + "]");
        }

        System.out.print("\n====================TASK4====================\n");
        String[] array = {"abc", "foo", "java"};
        String value1 = "hello";
        String[] array2 = {"abc", "def", "123"};
        String value2 = "Abc";
        String[] array3 = {"abc", "def", "123", "Java", "Hello"};
        String value3 = "123";

        boolean result1 = containsValue(array, value1);
        boolean result2 = containsValue(array2, value2);
        boolean result3 = containsValue(array3, value3);

        System.out.println(result1);
        System.out.println(result2);
        System.out.println(result3);

        System.out.print("\n====================TASK5====================\n");
        System.out.println(reverseSentence("Hello"));
        System.out.println(reverseSentence("Java is fun"));
        System.out.println(reverseSentence("This is a sentence"));

        System.out.print("\n====================TASK6====================\n");
        System.out.println(removeStringSpecialsDigits("123Java #$%is fun"));
        System.out.println(removeStringSpecialsDigits("Selenium"));
        System.out.println(removeStringSpecialsDigits("Selenium 123#$%Cypress"));

        System.out.print("\n====================TASK7====================\n");
        System.out.println(Arrays.toString(removeArraySpecialsDigits(new String[]{"123Java", "#$%is", "fun"})));
        System.out.println(Arrays.toString(removeArraySpecialsDigits(new String[]{"Selenium", "123$%", "###"})));
        System.out.println(Arrays.toString(removeArraySpecialsDigits(new String[]{"Selenium", "123#$%Cypress"})));

        System.out.print("\n====================TASK8====================\n");
        System.out.println(removeAndReturnCommons(new ArrayList<>(Arrays.asList("Java", "is", "fun")),
                new ArrayList<>(Arrays.asList("abc","xyz", "123"))));
        System.out.println(removeAndReturnCommons(new ArrayList<>(Arrays.asList("Java", "is", "fun")),
                new ArrayList<>(Arrays.asList("Java","C#", "Python"))));
        System.out.println(removeAndReturnCommons(new ArrayList<>(Arrays.asList("Java", "C#", "C#")),
                new ArrayList<>(Arrays.asList("Python","C#", "C++"))));

        System.out.print("\n====================TASK9====================\n");
        System.out.println(noXInVariables(new ArrayList<>(Arrays.asList("abc", "123", "#$%"))));
        System.out.println(noXInVariables(new ArrayList<>(Arrays.asList("xyz", "123", "#$%"))));
        System.out.println(noXInVariables(new ArrayList<>(Arrays.asList("x", "123", "#$%"))));
        System.out.println(noXInVariables(new ArrayList<>(Arrays.asList("xyXyxy", "Xx", "ABC"))));

    }


}