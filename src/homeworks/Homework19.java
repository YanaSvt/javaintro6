package homeworks;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class Homework19 {
    //TASK01

    /**
     *Write a method called as sum() that takes an int array and a boolean  and returns the sum of the numbers positioned at even-odd indexes based on boolean value.
     * NOTE: if the boolean value is true, the method should return the sum of the elements that have even indexes.
     * If the boolean value is false, the method should return the sum of the elements that have odd indexes.
     */
    public static int sum(int[] numbers, boolean isEvenIndexes) {
        int sum = 0;
        for (int i = 0; i < numbers.length; i++) {
            if (isEvenIndexes && i % 2 == 0) {
                sum += numbers[i];
            } else if (!isEvenIndexes && i % 2 != 0) {
                sum += numbers[i];
            }
        }
        return sum;
    }

    //TASK02

    /**
     * Write a method called as nthChars() that takes a String and an int arguments and returns the String back with every nth characters.
     * NOTE: your method should return empty String if the length of given String is less than the given number.
     */
    public static String nthChars(String input, int n) {
        if (input.length() < n) {
            return ""; // Return empty String if length is less than n
        }

        StringBuilder stringBuilder = new StringBuilder();

        for (int i = n - 1; i < input.length(); i += n) {
            stringBuilder.append(input.charAt(i));
        }

        return stringBuilder.toString();
    }

    //TASK03

    /**
     * Write a method canFormString() that takes 2 String arguments and returns true if the second String can be formed by rearranging the characters of first String. Return false otherwise.
     * NOTE: This method is case-insensitive and should ignore the white spaces!
     */
    public static boolean canFormString(String firstString, String secondString) {
        // Remove whitespace and convert to lowercase
        firstString = firstString.replaceAll("\\s", "").toLowerCase();
        secondString = secondString.replaceAll("\\s", "").toLowerCase();

        // Create frequency map for the characters in the first string
        Map<Character, Integer> frequencyMap = new HashMap<>();

        for (char c : firstString.toCharArray()) {
            frequencyMap.put(c, frequencyMap.getOrDefault(c, 0) + 1);
        }

        // Check if the second string can be formed by rearranging characters of the first string
        for (char c : secondString.toCharArray()) {
            if (!frequencyMap.containsKey(c) || frequencyMap.get(c) <= 0) {
                return false;
            }
            frequencyMap.put(c, frequencyMap.get(c) - 1);
        }
        return true;
    }

    //TASK04

    /**
     * Write a method isAnagram() that takes 2 String arguments and returns true if the given Strings are anagram. Return false otherwise.
     * NOTE:An anagram is a word or phrase formed by rearranging the letters of another word or phrase. In the context of strings, checking if two strings are anagrams of each other means verifying if they contain the same characters in the same quantities, regardless of the order of those characters.
     * NOTE: This method is case-insensitive and should ignore the white spaces!
     */
    public static boolean isAnagram(String firstString, String secondString) {
        firstString = firstString.replaceAll("\\s", "").toLowerCase();
        secondString = secondString.replaceAll("\\s", "").toLowerCase();

        char[] firstChars = firstString.toCharArray();
        char[] secondChars = secondString.toCharArray();
        Arrays.sort(firstChars);
        Arrays.sort(secondChars);

        return Arrays.equals(firstChars, secondChars);
    }


    public static void main(String[] args) {
        System.out.print("\n====================TASK1====================\n");
       /* int[] numbers = {1, 5,10};
        boolean isEvenIndexes = true;
        int result = sum(numbers, isEvenIndexes);
        System.out.println(result);*/

        int[] numbers = {3, 7, 2, 5, 10};
        boolean isEvenIndexes = false;
        int result = sum(numbers, isEvenIndexes);
        System.out.println(result);

        System.out.print("\n====================TASK2====================\n");
        System.out.println(nthChars("Java", 2));
        System.out.println(nthChars("JavaScript", 5));
        System.out.println(nthChars("Java", 3));
        System.out.println(nthChars("Hi", 4));

        System.out.print("\n====================TASK3====================\n");
        System.out.println(canFormString("Hello", "Hi"));
        System.out.println(canFormString("halogen", "hello"));
        System.out.println(canFormString("programming", "gaming"));
        System.out.println(canFormString("CONVERSATION", "voices rant on"));

        System.out.print("\n====================TASK4====================\n");
        System.out.println(isAnagram("Apple", "Peach"));
        System.out.println(isAnagram("listen", "silent"));
        System.out.println(isAnagram("astronomer", "moon starer"));
        System.out.println(isAnagram("CINEMA", "iceman"));


    }
}