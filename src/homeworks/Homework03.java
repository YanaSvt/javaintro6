package homeworks;

import java.util.Scanner;

public class Homework03 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("\n====================TASK1====================\n");

        System.out.println("Please enter 2 numbers");
        int num1 = input.nextInt();
        int num2 = input.nextInt();
        System.out.println("The difference between numbers is = " + Math.abs(num1 - num2));

        System.out.print("\n====================TASK2====================\n");
        System.out.println("Please enter 5 numbers");
        int number1 = input.nextInt();
        int number2 = input.nextInt();
        int number3 = input.nextInt();
        int number4 = input.nextInt();
        int number5 = input.nextInt();

        System.out.println("Max value = " + Math.max(Math.max(Math.max(number1, number2), Math.max(number3, number4)), number5));
        System.out.println("Min value = " + Math.min(Math.min(Math.min(number1, number2), Math.min(number3, number4)), number5));

        System.out.print("\n====================TASK3====================\n");
        int ran1 = (int) (Math.random() * 51) + 50;
        int ran2 = (int) (Math.random() * 51) + 50;
        int ran3 = (int) (Math.random() * 51) + 50;

        System.out.println("Number 1 = " + ran1);
        System.out.println("Number 2 = " + ran2);
        System.out.println("Number 3 = " + ran3);

        System.out.println("The sum of numbers is = " + (ran1 + ran2 + ran3));

        System.out.print("\n====================TASK4====================\n");
        double alexMoney = 125, mikeMoney = 220, alexToMike = 25.5;

        System.out.println("Alex’s money: $" + (alexMoney - alexToMike));
        System.out.println("Mike’s money: $" + (mikeMoney + alexToMike));

        System.out.print("\n====================TASK5====================\n");
        double bicycleCost = 390;
        double dailySavings = 15.60;

        //System.out.println("David can buy a bicycle in " + (int)(bicycleCost / dailySavings) + " days");
        System.out.println((int) (bicycleCost / dailySavings));

        System.out.print("\n====================TASK6====================\n");
        String s1 = "5", s2 = "10";
        System.out.println("-Sum of 5 and 10 is = " + (Integer.parseInt(s1) + Integer.parseInt(s2)));
        System.out.println("-Product of 5 and 10 is = " + (Integer.parseInt(s1) * Integer.parseInt(s2)));
        System.out.println("-Division of 5 and 10 is = " + (Integer.parseInt(s1) / Integer.parseInt(s2)));
        System.out.println("-Subtraction of 5 and 10 is = " + (Integer.parseInt(s1) - Integer.parseInt(s2)));
        System.out.println("-Remainder of 5 and 10 is = " + (Integer.parseInt(s1) % Integer.parseInt(s2)));

        System.out.print("\n====================TASK7====================\n");
        String str1 = "200", str2 = "-50";
        System.out.println("The greatest value is = " + Math.max(Integer.parseInt(str1), Integer.parseInt(str2)));
        System.out.println("The smallest value is = " + Math.min(Integer.parseInt(str1), Integer.parseInt(str2)));
        System.out.println("The average is = " + (Integer.parseInt(str1) + Integer.parseInt(str2)) / 2);
        System.out.println("The absolute difference is = " + Math.abs(Integer.parseInt(str1) - Integer.parseInt(str2)));

        System.out.print("\n====================TASK8====================\n");
        double _3quarters = 0.75;
        double _1dime = 0.10;
        double _2nickels = 0.10;
        double _1penny = 0.01;

        double planSavings1 = 24;
        double planSavings2 = 168;
        double daySavings = _3quarters + _1dime + _1penny + _2nickels;// 0.96

        System.out.println((int) (planSavings1 / daySavings) + " days");
        System.out.println((int) (planSavings2 / daySavings) + " days");
        System.out.println("$" + daySavings * (5 * 30));

        System.out.print("\n====================TASK9====================\n");
        double computerCost = 1250;
        double dailySavingsJessie = 62.5;

        System.out.println((int) (computerCost / dailySavingsJessie));

        System.out.print("\n====================TASK10====================\n");
        double newCar = 14265, option1 = 475.50, option2 = 951;

        System.out.println("Option 1 will take " + (int) (newCar / option1) + " months");
        System.out.println("Option 2 will take " + (int) (newCar / option2) + " months");

        System.out.print("\n====================TASK11====================\n");
        System.out.println("Please enter 2 numbers");
        int enteredNumber1 = input.nextInt(), enteredNumber2 = input.nextInt();
        double result = (double) enteredNumber1 / (double) enteredNumber2;

        System.out.println(result);

        System.out.print("\n====================TASK12====================\n");
        int randomNumber1 = (int) (Math.random() * 101);
        int randomNumber2 = (int) (Math.random() * 101);
        int randomNumber3 = (int) (Math.random() * 101);
        //System.out.println(randomNumber1);
        //System.out.println(randomNumber2);
        //System.out.println(randomNumber3);

        if (randomNumber1 > 25 && randomNumber2 > 25 && randomNumber3 > 25) {
            System.out.println("true");
        } else {
            System.out.println("false");
        }
        //System.out.println("End of the program");

        System.out.print("\n====================TASK13====================\n");
        System.out.println("Please enter a number between 1 to 7 (1 and 7 are included)");
        int numWeek = input.nextInt();

        if (numWeek == 1) {
            System.out.println("The number entered returns MONDAY");
        } else if (numWeek == 2) {
            System.out.println("The number entered returns TUESDAY");
        } else if (numWeek == 3) {
            System.out.println("The number entered returns WEDNESDAY");
        } else if (numWeek == 4) {
            System.out.println("The number entered returns THURSDAY");
        } else if (numWeek == 5) {
            System.out.println("The number entered returns FRIDAY");
        } else if (numWeek == 6) {
            System.out.println("The number entered returns SATURDAY");
        } else if (numWeek == 7) {
            System.out.println("The number entered returns SUNDAY");
        }
        /*else {
            System.out.println("Please enter a valid number from 1 to 7 only!");
        }*/

        System.out.print("\n====================TASK14====================\n");
        System.out.println("Tell me your exam results");
        int exam1 = input.nextInt(), exam2 = input.nextInt(), exam3 = input.nextInt();

        if ((exam1 + exam2 + exam3) / 3 >= 70) {
            System.out.println("YOU PASSED!");
        } else {
            System.out.println("YOU FAILED!");
        }

        System.out.print("\n====================TASK15====================\n");
        System.out.println("Enter 3 numbers");
        int sameDiffNum1 = input.nextInt(), sameDiffNum2 = input.nextInt(), sameDiffNum3 = input.nextInt();

        if (sameDiffNum1 == sameDiffNum2 && sameDiffNum2 == sameDiffNum3) {
            System.out.println("TRIPLE MATCH");
        } else if (sameDiffNum1 != sameDiffNum2 && sameDiffNum2 != sameDiffNum3 && sameDiffNum1 != sameDiffNum3) {
            System.out.println("NO MATCH");
        } else {
            System.out.println("DOUBLE MATCH");
        }


    }
}
