package homeworks;

public class Homework14 {
    //TASK01
    /**
     * -Create a method called fizzBuzz1()
     * -This method will take an int argument, and it will print numbers starting from 1 to given argument.
     * BUT, it will print “Fizz” for the numbers divided by 3, “Buzz” for the numbers divided by 5, and “FizzBuzz” for the numbers divided both by 3 and 5.a
     */
    public static void fizzBuzz1(int a){
        for (int i = 1; i <= a; i++) {
            if (i % 15 == 0) System.out.println("FizzBuzz");
            else if (i % 3 == 0) System.out.println("Fizz");
            else if (i % 5 == 0) System.out.println("Buzz");
            else System.out.println(i);
        }
    }

    //TASK02
    /**
     * Create a method called fizzBuzz2()
     * -This method will take an int argument, and it will return a String. BUT it will return “Fizz” if the given number is divided by 3,
     * “Buzz” if the number is divided by 5, and “FizzBuzz” if the number is divided both by 3 and 5. Otherwise, it will return number itself.
     */
    public static String fizzBuzz2(int a){
            if (a % 15 == 0) return "FizzBuzz";
            else if (a % 3 == 0) return"Fizz";
            else if (a % 5 == 0) return "Buzz";
        else return Integer.toString(a);
    }

    //TASK03
    /**
     * Create a method called findSumNumbers()
     * -This method will take a String argument and it will return an int which is the sum of all numbers presented in the String.
     * NOTE: If there are no numbers represented in the String, return 0.
     */
    public static int findSumNumbers(String str){
        int sum = 0;

        // Regular expression pattern to match numbers
        String pattern = "\\d+";

        // Find all matches of numbers in the input string
        java.util.regex.Pattern regex = java.util.regex.Pattern.compile(pattern);
        java.util.regex.Matcher matcher = regex.matcher(str);

        // Iterate over the matches and sum up the numbers
        while (matcher.find()) {
            int number = Integer.parseInt(matcher.group());
            sum += number;
        }

        return sum;
    }

    //TASK04
    /**
     * Create a method called findBiggestNumber()
     * -This method will take a String argument and it will return an int which is the number presented in the String.
     * NOTE: If there are no numbers represented in the String, return 0.
     */
    public static int findBiggestNumber(String str){
        int biggestNumber = 0;

        // Regular expression pattern to match numbers
        String pattern = "\\d+";

        // Find all matches of numbers in the string
        java.util.regex.Pattern regex = java.util.regex.Pattern.compile(pattern);
        java.util.regex.Matcher matcher = regex.matcher(str);

        // Iterate over the matches and find the biggest number
        while (matcher.find()) {
            int number = Integer.parseInt(matcher.group());
            biggestNumber = Math.max(biggestNumber, number);
        }
        return biggestNumber;
    }

    //TASK05
    /**
     * Create a method called countSequenceOfCharacters()
     * -This method will take a String argument and it will return a String which is the count of repeated characters in a sequence in the String.
     * NOTE: If given String is empty, then return empty String.
     * NOTE: It is case sensitive!!!
     */
    public static String countSequenceOfCharacters(String str){
        if(str.isEmpty()) return str;
        String result = "";
        char letter = str.charAt(0);
        int count = 1;

        for (int i = 1; i < str.length(); i++) {
            if(str.charAt(i) == letter) count++;
            else{
                result += Integer.toString(count) + letter;
                letter = str.charAt(i);
                count = 1;
            }
        }

        result += Integer.toString(count) + letter;
        return result;
    }


    public static void main(String[] args) {
        System.out.print("\n====================TASK1====================\n");
        fizzBuzz1(3);
        fizzBuzz1(5);
        fizzBuzz1(18);

        System.out.print("\n====================TASK2====================\n");
        System.out.println(fizzBuzz2(0));
        System.out.println(fizzBuzz2(1));
        System.out.println(fizzBuzz2(3));
        System.out.println(fizzBuzz2(5));
        System.out.println(fizzBuzz2(15));

        System.out.print("\n====================TASK3====================\n");
        System.out.println(findSumNumbers("abc$”"));
        System.out.println(findSumNumbers("a1b4c  6#”"));
        System.out.println(findSumNumbers("ab110c045d"));
        System.out.println(findSumNumbers("525"));

        System.out.print("\n====================TASK4====================\n");
        System.out.println(findBiggestNumber("abc$”"));
        System.out.println(findBiggestNumber("a1b4c  6#”"));
        System.out.println(findBiggestNumber("ab110c045d"));
        System.out.println(findBiggestNumber("525"));

        System.out.print("\n====================TASK5====================\n");
        System.out.println(countSequenceOfCharacters(""));
        System.out.println(countSequenceOfCharacters("abc"));
        System.out.println(countSequenceOfCharacters("abbcca"));
        System.out.println(countSequenceOfCharacters("aaAAa"));


    }

}
