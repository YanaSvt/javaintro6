package homeworks;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class Homework07 {
    public static void main(String[] args) {
        System.out.print("\n====================TASK1====================\n");
        /*
-Create an ArrayList and store below numbers
10, 23, 67, 23, 78
THEN:
-Print element at index of 3
-Print element at index of 0
-Print element at index of 2
-Print the entire list
Expected Result:
23
10
67
[10, 23, 67, 23, 78
         */
        ArrayList<Integer> numbers = new ArrayList<>(Arrays.asList(10, 23, 67, 23, 78));
        System.out.println(numbers.get(3));
        System.out.println(numbers.get(0));
        System.out.println(numbers.get(2));
        System.out.println(numbers);

        System.out.print("\n====================TASK2====================\n");
        /*
-Create an ArrayList and store below colors
Blue, Brown, Red, White, Black, Purple
THEN:
-Print element at index of 1
-Print element at index of 3
-Print element at index of 5
-Print the entire list
Expected Result:
Brown
White
Purple
[Blue, Brown, Red, White, Black, Purple]
         */
        ArrayList<String> colors = new ArrayList<>(Arrays.asList("Blue", "Brown", "Red", "White", "Black", "Purple"));
        System.out.println(colors.get(1));
        System.out.println(colors.get(3));
        System.out.println(colors.get(5));
        System.out.println(colors);

        System.out.print("\n====================TASK3====================\n");
        /*
        -Create an ArrayList and store below numbers
23, -34, -56, 0, 89, 100
THEN:
-Print the entire list
-Print the entire list sorted in ascending order
Expected Result:
[23, -34, -56, 0, 89, 100]
[-56, -34, 0, 23, 89, 100]
         */
        ArrayList<Integer> nums = new ArrayList<>(Arrays.asList(23, -34, -56, 0, 89, 100));
        System.out.println(nums);
        Collections.sort(nums);
        System.out.println(nums);

        System.out.print("\n====================TASK4====================\n");
        /*
        -Create an ArrayList and store below cities
Istanbul, Berlin, Madrid, Paris
THEN:
-Print the entire list
-Print the entire list sorted lexicographically
Expected Result:
[Istanbul, Berlin, Madrid, Paris]
[Berlin, Istanbul, Madrid, Paris]
         */
        ArrayList<String> cities = new ArrayList<>(Arrays.asList("Istanbul", "Berlin", "Madrid", "Paris"));
        System.out.println(cities);
        Collections.sort(cities);
        System.out.println(cities);

        System.out.print("\n====================TASK5====================\n");
        /*
        Create an ArrayList and store Marvel characters below
Spider Man, Iron Man, Black Panter, Deadpool, Captain America
THEN:
-Print the entire list
-Then, check if it contains Wolwerine
if it contains Wolwerine, then print true
if it does not contain Wolwerine, print false
Expected Result:
[Spider Man, Iron Man, Black Panter, Deadpool, Captain America]
false
         */
        ArrayList<String> marvel = new ArrayList<>(Arrays.asList("Spider Man", "Iron Man", "Black Panter", "Deadpool", "Captain America"));
        System.out.println(marvel);
        for (String marv: marvel) {
            System.out.println(marv.contains("Wolwerine"));
            break;
        }

        System.out.print("\n====================TASK6====================\n");
        /*
        Create an ArrayList and store Avengers characters below
Hulk, Black Widow, Captain America, Iron Man

THEN:
-Print the entire list sorted lexicographically
-Then, check if it contains Hulk and Iron Man
	if it contains both, then print true
	if it does not contain both, print false

         */
        ArrayList<String> avengers = new ArrayList<>(Arrays.asList("Hulk", "Black Widow", "Captain America", "Iron Man"));
        Collections.sort(avengers);
        System.out.println(avengers);

        System.out.println(avengers.contains("Hulk") && avengers.contains("Iron Man"));

        System.out.print("\n====================TASK7====================\n");
        /*
        Create an ArrayList and store characters below
A, x, $, %, 9, *, +, F, G
THEN:
-Print the entire list
-Print each element
Expected Result:
[A, x, $, %, 9, *, +, F, G]
A
x
$
%
9
*
+
F
G
         */
        ArrayList<Character> characters = new ArrayList<>(Arrays.asList('A', 'x', '$', '%', '9', '*', '+', 'F', 'G'));
        System.out.println(characters);
        System.out.println(characters.get(0));
        System.out.println(characters.get(1));
        System.out.println(characters.get(2));
        System.out.println(characters.get(3));
        System.out.println(characters.get(4));
        System.out.println(characters.get(5));
        System.out.println(characters.get(6));
        System.out.println(characters.get(7));
        System.out.println(characters.get(8));

        System.out.print("\n====================TASK8====================\n");
        /*
        Create an ArrayList and store below objects
Desk, Laptop, Mouse, Monitor, Mouse-Pad, Adapter
THEN:
-Print the entire list
-Print the entire list sorted lexicographically
-Count objects that starts with M or m
-Count objects that does not have A or a or E or e
Expected Result:
[Desk, Laptop, Mouse, Monitor, Mouse-Pad, Adapter]
[Adapter, Desk, Laptop, Monitor, Mouse, Mouse-Pad]
3
1
         */
        ArrayList<String> equipment = new ArrayList<>(Arrays.asList("Desk", "Laptop", "Mouse", "Monitor", "Mouse-Pad", "Adapter"));
        System.out.println(equipment);
        Collections.sort(equipment);
        System.out.println(equipment);

        int countM = 0;
        for (String str : equipment) {
            if (str.toLowerCase().contains("m"))
                countM++;
        }
        System.out.println(countM);

        int noAE = 0;
        for (String str2: equipment) {
            if (!str2.toLowerCase().contains("a") && !str2.toLowerCase().contains("e"))
                noAE++;
        }
        System.out.println(noAE);

        System.out.print("\n====================TASK9====================\n");
        /*
        -Create an ArrayList and store below kitchen objects
Plate, spoon, fork, Knife, cup, Mixer

THEN:
-Print the entire list
-Print how many elements starts with uppercase
-Print how many elements starts with lowercase
-Print how many elements has P or p
-Print how many elements starts or ends with P or p

Expected Result:
[Plate, spoon, fork, Knife, cup, Mixer]
Elements starts with uppercase = 3
Elements starts with lowercase = 3
Elements having P or p= 3
Elements starting or ending with P or p = 2
         */
        ArrayList<String> kitchen = new ArrayList<>(Arrays.asList("Plate", "spoon", "fork", "Knife", "cup", "Mixer"));
        System.out.println(kitchen);

        int upper = 0;
        int lower = 0;

        for (String kitch: kitchen) {
            char c = kitch.charAt(0);
            if(Character.isUpperCase(c)) upper++;
            else if(Character.isLowerCase(c)) lower++;
        }
        System.out.println("Elements starts with uppercase = " + upper);
        System.out.println("Elements starts with lowercase = " + lower);

        int countP = 0;
        for (String k : kitchen) {
            if (k.toLowerCase().contains("p")) countP++;
        }
        System.out.println("Elements having P or p = " + countP);

        int startEndP = 0;
        for (String kit : kitchen) {
            if (kit.toLowerCase().startsWith("p") || kit.toLowerCase().endsWith("p")) startEndP++;
        }
        System.out.println("Elements starting or ending with P or p = " + startEndP);

        System.out.print("\n====================TASK10====================\n");
        /*
        Create an ArrayList and store below numbers
3, 5, 7, 10, 0, 20, 17, 10, 23, 56, 78

THEN:
-Print the entire list
-Print how many element can be divided by 10
-Print how many even numbers are greater than 15
-Print how many odd numbers are less than 20
-Print how many elements are less than 15 or greater than 50

Expected Result:
[3, 5, 7, 10, 0, 20, 17, 10, 23, 56, 78]
Elements that can be divided by 10 = 4
Elements that are even and greater than 15 = 3
Elements that are odd and less than 20 = 4
Elements that are less than 15 or greater than 50 = 8

         */
        ArrayList<Integer> numb = new ArrayList<>(Arrays.asList(3, 5, 7, 10, 0, 20, 17, 10, 23, 56, 78));
        System.out.println(numb);

        int dividedBy10 = 0;
        for(Integer n : numb){
            if(n % 10 == 0) dividedBy10++;
        }
        System.out.println("Elements that can be divided by 10 = " + dividedBy10);

       int evenGreater15 = 0;
        for(Integer n : numb){
             if(n % 2 == 0 && n > 15) evenGreater15++;
        }
        System.out.println("Elements that are even and greater than 15 = " + evenGreater15);

        int oddLess20 = 0;
        for(Integer n : numb){
            if(n % 2 != 0 && n < 20) oddLess20++;
        }
        System.out.println("Elements that are odd and less than 20 = " + oddLess20);

        int less15OrGreater50 = 0;
        for(Integer n : numb){
            if(n <15 || n > 50) less15OrGreater50++;
        }
        System.out.println("Elements that are less than 15 or greater than 50 =  " + less15OrGreater50);








    }
}
