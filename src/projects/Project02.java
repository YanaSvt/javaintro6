package projects;

import com.sun.xml.internal.ws.addressing.WsaActionUtil;

import java.util.Date;
import java.util.Scanner;

public class Project02 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

       System.out.print("\n====================TASK1====================\n");
        System.out.println("Please enter 3 numbers ");
        int num1 = input.nextInt();
        int num2 = input.nextInt();
        int num3 = input.nextInt();

        System.out.println("The product of the numbers entered is = " + num1 * num2 * num3);

        System.out.print("\n====================TASK2====================\n");
        System.out.println("What is your first name?");
        String name = input.next();

        System.out.println("What is your last name?");
        String lastName = input.next();

        System.out.println("What is your year of birth?");
        short yearOfBirth = input.nextShort();
        input.nextLine(); // to fix scanner bug

        Date dt = new Date();
        int year = dt.getYear();
        int currentYear = year + 1900;

        System.out.println(name + " " + lastName + "-s age is = " + (currentYear - yearOfBirth) + ".");

        System.out.print("\n====================TASK3====================\n");
        System.out.println("What is your full name?");
        String fullName = input.nextLine();


        System.out.println("What is your weight in kg?");
        double kgWeight = input.nextDouble();
        input.nextLine(); // to fix scanner bug

        System.out.println(fullName + "’s weight is = " + kgWeight * 2.205 + " lbs.");

        System.out.print("\n====================TASK4====================\n");
        System.out.println("What is your full name?");
        String fullName1 = input.nextLine();

        System.out.println("What is your age?");
        int age1 = input.nextInt();
        input.nextLine(); // to fix scanner bug

        System.out.println("What is your full name?");
        String fullName2 = input.nextLine();
        System.out.println("What is your age?");
        int age2 = input.nextInt();
        input.nextLine(); // to fix scanner bug

        System.out.println("What is your full name?");
        String fullName3 = input.nextLine();
        System.out.println("What is your age?");
        int age3 = input.nextInt();

        System.out.println(fullName1 + "’s age is " + age1 + ".");
        System.out.println(fullName2 + "’s age is " + age2 + ".");
        System.out.println(fullName3 + "’s age is " + age3 + ".");

        System.out.println("The average age is " + (age1 + age2 + age3) / 3 + ".");
        System.out.println("The eldest age is " + Math.max(Math.max(age1, age2), age3) + ".");
        System.out.println("The youngest age is " + Math.min(Math.min(age1, age2), age3) + ".");

    }
}
