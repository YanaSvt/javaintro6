package projects;

public class Project03 {
    public static void main(String[] args) {
       System.out.print("\n====================TASK1====================\n");// string to primitive
        String s1 = "24", s2 = "5";
        System.out.println("The sum of " + s1 + " and " + s2 + " = " + (Integer.parseInt(s1) + Integer.parseInt(s2)));
        System.out.println("The subtraction of " + s1 + " and " + s2 + " = " + (Integer.parseInt(s1) - Integer.parseInt(s2)));
        System.out.println("The division of " + s1 + " and " + s2 + " = " + (Double.parseDouble(s1) / Integer.parseInt(s2)));
        System.out.println("The multiplication of " + s1 + " and " + s2 + " = " + (Integer.parseInt(s1) * Integer.parseInt(s2)));
        System.out.println("The remainder of " + s1 + " and " + s2 + " = " + (Integer.parseInt(s1) % Integer.parseInt(s2)));

        System.out.print("\n====================TASK2====================\n");
        int rand = (int)(Math.random() * 35)+1;

        System.out.println(rand);
        if (rand == 1) System.out.println(rand + " IS NOT A PRIME NUMBER");
        else if (rand == 9) System.out.println(rand + " IS NOT A PRIME NUMBER");
        else if (rand == 11) System.out.println(rand + " IS NOT A PRIME NUMBER");
        else if (rand == 15) System.out.println(rand + " IS NOT A PRIME NUMBER");
        else if (rand == 21) System.out.println(rand + " IS NOT A PRIME NUMBER");
        else if (rand == 25) System.out.println(rand + " IS NOT A PRIME NUMBER");
        else if (rand == 27) System.out.println(rand + " IS NOT A PRIME NUMBER");
        else if (rand == 33) System.out.println(rand + " IS NOT A PRIME NUMBER");
        else if (rand == 35) System.out.println(rand + " IS NOT A PRIME NUMBER");
        else if (rand == 2) System.out.println(rand + " IS A PRIME NUMBER");
        else if (rand % 2 == 1) System.out.println(rand + " IS A PRIME NUMBER");
        else System.out.println(rand + " IS NOT A PRIME NUMBER");

        System.out.print("\n====================TASK3====================\n");
        int n1 = (int)(Math.random()*50) + 1;
        int n2 = (int)(Math.random()*50) + 1;
        int n3 = (int)(Math.random()*50) + 1;

        System.out.println(n1);
        System.out.println(n2);
        System.out.println(n3);

        int max = Math.max(Math.max(n1,n2), n3);
        int min = Math.min(Math.min(n1,n2), n3);

        System.out.println("Lowest number is = " + min);
        if (n1 != min && n1 != max) System.out.println("Middle number is =  " + n1);
        else if (n2 != min && n2 != max) System.out.println("Middle number is =  " + n2);
        else if (n3 != min && n3 != max) System.out.println("Middle number is =  " + n3);
        System.out.println("Greatest number is = " + max);

        System.out.print("\n====================TASK4====================\n");
        char c = 'R';

        if (c >= 65 && c <= 90) System.out.println("The letter is uppercase");
        else if (c >= 97 && c <= 122) System.out.println("The letter is lowercase");
        else System.out.println("Invalid character detected!!!");

        System.out.print("\n====================TASK5====================\n");
        char ch = 'R';

        if ((ch >= 65 && ch <= 90) || (ch >= 97 && ch <= 122)){
            if (ch == 97 || ch == 101 || ch == 105 || ch ==111 || ch == 117 || ch == 65 || ch == 69 || ch == 73 || ch == 79 || ch == 85)System.out.println("The letter is vowel");
            else System.out.println("The letter is consonant");
        }
        else System.out.println("Invalid character detected!!!");

        System.out.print("\n====================TASK6====================\n");
        char given = '*';

        if ((given >= 32 && given <=47) || (given >= 58 && given <= 64) || (given >= 91 && given <= 96) || (given >= 123 && given <= 127))
            System.out.println("Special character is = " + given);
        else System.out.println("Invalid character detected!!!");

        System.out.print("\n====================TASK7====================\n");
        char givChar = '@';

        if((givChar >= 65 && givChar <= 90) || (givChar >= 97 && givChar <= 122)) System.out.println("Character is a letter");
        else if (givChar >= 48 && givChar <= 57) System.out.println("Character is a digit");
        else System.out.println("Character is a special character");



    }
}
