package projects;

import utilities.RandomGenerator;
import utilities.ScannerHelper;

import java.util.Scanner;

public class Project05 {
    public static void main(String[] args) {
       System.out.print("\n====================TASK1====================\n");
        String str = ScannerHelper.getString();
        if(str == null || str.isEmpty() || !str.contains(" ") ) {
            System.out.println("This sentence does not have multiple words");
        }
        else{
            int count = 0;
            for (int e = 0; e < str.length(); e++) {
                if (str.charAt(e) != ' ') {
                    count++;
                    while (str.charAt(e) != ' ' && e < str.length() - 1) {
                        e++;
                    }
                }
            }
            System.out.println("This sentence has " + count + " words.");
        }

        System.out.print("\n====================TASK2====================\n");
        int num1 = RandomGenerator.getRandomNumber(0,25);
        //System.out.println(num1);
        int num2 = RandomGenerator.getRandomNumber(0,25);
       // System.out.println(num2);
        String solution = "";

        for (int i = Math.min(num1, num2); i < Math.max(num1, num2); i++) {
            if (i % 5 == 0) continue;
            else solution += i + " - ";
        }
        System.out.println(solution.substring(0,solution.length() - 3));

        System.out.print("\n====================TASK3====================\n");
        String sentence = ScannerHelper.getString();
        int count = 0;

        for (int i = 0; i <= sentence.length()-1; i++) {
             if(sentence.toLowerCase().charAt(i) == 'a') count++;
        }
        if (sentence == null || sentence.isEmpty() || sentence.equals(" ")) System.out.println("This sentence does not have any characters");
        else System.out.println("This sentence has " + count + " a or A letters.");


        System.out.print("\n====================TASK4====================\n");
        Scanner input = new Scanner(System.in);
        System.out.println("Please enter a word");
        String word  = input.nextLine();
        String reversedName = "";

        for(int i = word.length()-1; i >= 0; i--){
            reversedName += word.charAt(i);
        }
        if (word.length()<1 || word.equals(" ")) System.out.println("This word does not have 1 or more characters");
        else if (word.equals(reversedName)) System.out.println("This word is palindrome");
        else System.out.println("This word is not palindrome");

        System.out.print("\n====================TASK5====================\n");

        for (int i = 0; i <= 9; i++) {//outer for loop for rows
            for (int j = 0; j <= (9 - i) * 2; j++) {//inner loop
                System.out.print(" ");//create space for pyramid shape
            }
            for (int k = i; k >= 1; k--) {//inner for loop
                System.out.print(" " + "*");//create left half
            }
            for (int l = 2; l <= i; l++) {//inner for loop
                System.out.print(" " + "*");    //create right half
            }//end outer for loop
            System.out.println();
        }






    }
}
