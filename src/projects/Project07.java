package projects;

import java.util.ArrayList;
import java.util.Arrays;

public class Project07 {
    public static void main(String[] args) {
        System.out.print("\n====================TASK1====================\n");
        System.out.println(countMultipleWords(new String[]{"foo", "", " ", "foo bar", "java is fun", " ruby "}));//2

        System.out.print("\n====================TASK2====================\n");
        System.out.println(removeNegatives(new ArrayList<>(Arrays.asList(2, -5, 6, 7, -10, -78, 0, 15))));

        System.out.print("\n====================TASK3====================\n");
        System.out.println(validatePassword(" "));
        System.out.println(validatePassword("abcd"));
        System.out.println(validatePassword("abcd1234"));
        System.out.println(validatePassword("Abcd1234"));
        System.out.println(validatePassword("Abcd123!"));

       System.out.print("\n====================TASK4====================\n");
        System.out.println(validateEmailAddress("a@gmail.com"));
        System.out.println(validateEmailAddress("abc@g.com"));
        System.out.println(validateEmailAddress("abc@gmail.c"));
        System.out.println(validateEmailAddress("abc@@gmail.com"));
        System.out.println(validateEmailAddress("abcd@gmail.com"));
    }

    //TASK 1
    /*
    •	Write a method that takes a String[] array as an argument and counts how many strings in the array has multiple words.
•	This method will return an int which is the count of elements that have multiple words.
•	NOTE: be careful about these as they are not multiple words ->“”,    “   “,    “    abc”,  “abc   “
     */
    public static int countMultipleWords(String [] arr){
       int count = 0;
        for (String str: arr) {
            str = str.trim();
           if(str.contains(" ")) count++;
        }
        return count;
    }

    //TASK 2
    /*
    •	Write a method that takes an “ArrayList<Integer> numbers” as an argument and removes all negative numbers from the given list if there are any.
•	This method will return an ArrayList with removed negatives.

     */
    public static ArrayList<Integer> removeNegatives(ArrayList<Integer> list){
        list.removeIf(element -> element < 0);
        return list;
    }

    //TASK 3
    /*
    •	Write a method that takes a “String password” as an argument and checks if the given password is valid or not.
•	This method will return true if given password is valid, or false if given password is not valid.
•	A VALID PASSWORD:
	-should have length of 8 to 16 (both inclusive).
	-should have at least 1 digit, 1 uppercase, 1 lowercase and 1 special char.
	-should NOT have any space.

     */
    public static boolean validatePassword(String password){

            if (password.length() < 8 || password.length() > 16 || password.contains(" ")){
                return false;
            }else {
                int digit=0;
                int special=0;
                int upCount=0;
                int loCount=0;
                for(int i =0;i<password.length();i++){
                    char c = password.charAt(i);
                    if(Character.isUpperCase(c)){
                        upCount++;
                    }
                    else if(Character.isLowerCase(c)){
                        loCount++;
                    }
                    else if(Character.isDigit(c)){
                        digit++;
                    }
                    else if(!Character.isLetterOrDigit(c)){
                        special++;
                    }
                }
                return special >= 1 && loCount >= 1 && upCount >= 1 && digit >= 1;
        }
    }

    //TASK 4
    /*
    •	Write a method that takes a “String email” as an argument and checks if the given email is valid or not.
•	This method will return true if given email is true, or false if given email is not valid.
•	A VALID EMAIL:
	-should NOT have any space.
	-should not have more than one “@” character.
	-should be in the given format <2+chars>@<2+chars>.<2+chars>

     */
    public static boolean validateEmailAddress(String email){
        int countAtSign = 0;
        int countDot = 0;
        if(email.contains(" ")){
            return false;
        }
        else if(!email.contains("@")){
            return false;
        }
        else if(!email.contains(".")){
            return false;
        }
        for(int i = 0; i < email.length(); i++){
            if(email.charAt(i) == 64){
                countAtSign++;
            }
            else if(email.charAt(i) == 46){
                countDot++;
            }
        }
        if(countAtSign == 0 || countAtSign > 1){
            return false;
        }
        if(countDot == 0 || countDot > 1){
            return false;
        }
        String[] arr1 = email.split("\\.", 2);
        String[] arr2 = arr1[0].split("@" , 2);

        if(arr1[0].length() >= 2 && arr1[1].length() >= 2 && arr2[0].length() >= 2 && arr2[1].length() >= 2){
            return true;
        }
        else{
            return false;
        }
    }



}
