package projects;

public class Project01 {
    public static void main(String[] args) {
        System.out.print("\n====================TASK1====================\n");

        String name = "Yana";//declaring and initializing
        System.out.println("My name is" + " " + name);//or "My name is " + name

        System.out.print("\n====================TASK2====================\n");

        char nameCharacter1, nameCharacter2, nameCharacter3, nameCharacter4; //declaring variables
        nameCharacter1 = 'Y'; // initializing variables
        nameCharacter2 = 'a';
        nameCharacter3 = 'n';
        nameCharacter4 = 'a';
        System.out.println("Name letter 1 is " + nameCharacter1 + "\n" + "Name letter 2 is " + nameCharacter2 + "\n" + "Name letter 3 is " + nameCharacter3 + "\n" + "Name letter 4 is " + nameCharacter4);

        System.out.print("\n====================TASK3====================\n");

        String myFavMovie = "Avatar", myFavSong = "September", myFavCity = "Chicago", myFavActivity = "Horse back riding", myFavSnack = "Pretzel"; //declaring and initiating multiple strings
        System.out.println("My favorite movie is " + myFavMovie);
        System.out.println("My favorite song is " + myFavSong);
        System.out.println("My favorite City is " + myFavCity);
        System.out.println("My favorite activity is " + myFavActivity);
        System.out.println("My favorite snack is " + myFavSnack);

        System.out.print("\n====================TASK4====================\n");

        int myFavNumber = 130, numberOfStatesIVisited = 7, numberOfCountriesIVisited = 8;
        System.out.println("My favorite number is " + myFavNumber);
        System.out.println("Number of States I visited is  " + numberOfStatesIVisited);
        System.out.println("Number of Countries I visited is  " + numberOfCountriesIVisited);

        System.out.print("\n====================TASK5====================\n");

        boolean amIAtSchoolToday = false;
        System.out.println("I am at school today = " + amIAtSchoolToday);

        System.out.print("\n====================TASK6====================\n");

        boolean isWeatherNiceToday = false;
        System.out.println("The weather is nice today = " + isWeatherNiceToday);





    }
}
