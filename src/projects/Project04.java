package projects;

import utilities.ScannerHelper;

import java.util.Scanner;

public class Project04 {
    public static void main(String[] args) {
        System.out.print("\n====================TASK1====================\n");
        String str = ScannerHelper.getString();
        if (str.length() < 8) System.out.println("This String does not have 8 characters");
        else
            System.out.println(str.substring(str.length() - 4) + str.substring(4, str.length() - 4) + str.substring(0, 4));

        System.out.print("\n====================TASK2====================\n");
        String st = ScannerHelper.getString();

        if (!st.contains(" ")) System.out.println("This sentence does not have 2 or more words to swap");
        else {
            int first = st.indexOf(' ');                             // first "space" character that occurs
            int last = st.lastIndexOf(' ');                          // last "space" character that occurs

            String firstWord = st.substring(0, first);               // substring first word from index 0  to index of first "space" character
            String lastWord = st.substring(last + 1);   // substring last word from index the of last "space" character to higher index of sentence
            String midSentece = st.substring(first, last);

            System.out.println(lastWord + midSentece + " " + firstWord);
        }

        System.out.print("\n====================TASK3====================\n");
        String str1 = "These books are so stupid";
        String str2 = "I like idiot behaviors";
        String str3 = "I had some stupid t-shirts in the past and also some idiot look shoes";

        if (str1.contains("stupid") && str1.contains("idiot"))
            System.out.println(str1.replace("stupid", "nice").replace("idiot", "nice"));
        else if (str1.contains("stupid") || str1.contains("idiot"))
            System.out.println(str1.replace("stupid", "nice").replace("idiot", "nice"));
        else System.out.println(str1);

        if (str2.contains("stupid") && str2.contains("idiot"))
            System.out.println(str2.replace("stupid", "nice").replace("idiot", "nice"));
        else if (str2.contains("stupid") || str2.contains("idiot"))
            System.out.println(str2.replace("stupid", "nice").replace("idiot", "nice"));
        else System.out.println(str2);

        if (str3.contains("stupid") && str3.contains("idiot"))
            System.out.println(str3.replace("stupid", "nice").replace("idiot", "nice"));
        else if (str3.contains("stupid") || str3.contains("idiot"))
            System.out.println(str3.replace("stupid", "nice").replace("idiot", "nice"));
        else System.out.println(str3);

        System.out.print("\n====================TASK4====================\n");
        String name = ScannerHelper.getFirstName();
        if (name.length() < 2) System.out.println("Invalid Input!!!");
        else if (name.length() % 2 == 0)
            System.out.println(name.substring(name.length() / 2 - 1, name.length() / 2 + 1));
        else System.out.println(name.charAt(name.length() / 2));

        System.out.print("\n====================TASK5====================\n");
        Scanner input = new Scanner(System.in);
        System.out.println("Please enter a Country");
        String country = input.nextLine();

        if (country.length() < 5) System.out.println("Invalid Input!!!");
        else System.out.println(country.substring(2, country.length() - 2));

        System.out.print("\n====================TASK6====================\n");
        String adr = ScannerHelper.getAddress();

        System.out.println(adr.replace("a", "*").replace("A", "*").replace("e", "#").replace("E", "#").replace("i", "+").replace("I", "+").replace("u", "$").replace("U", "$").replace("o", "@").replace("O", "@"));

        System.out.print("\n====================TASK7====================\n");
        Scanner inp = new Scanner(System.in);
        System.out.println("Please enter a sentence");
        String sentence = inp.nextLine();

        if (!sentence.trim().contains(" ")) System.out.println("This sentence does not have multiple words”.");
        else {
            String[] arr = sentence.split(" ");
            System.out.println("This sentence has " + arr.length + " words.");


        }
    }
}