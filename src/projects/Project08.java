package projects;


import java.util.*;

public class Project08 {
    public static void main(String[] args) {
        System.out.print("\n====================TASK1====================\n");
        System.out.println(findClosestDistance(new int[]{4}));
        System.out.println(findClosestDistance(new int[]{4, 8, 7, 15}));
        System.out.println(findClosestDistance(new int[]{10, -5, 20, 50, 100}));

        System.out.print("\n====================TASK2====================\n");
        System.out.println(findSingleNumber(new int[]{2}));
        System.out.println(findSingleNumber(new int[]{5, 3, -1, 3, 5, 7, -1}));

        System.out.print("\n====================TASK3====================\n");
        System.out.println(findFirstUniqueCharacter("Hello"));
        System.out.println(findFirstUniqueCharacter("abc abc d"));
        System.out.println(findFirstUniqueCharacter("aba"));

        System.out.print("\n====================TASK4====================\n");
        System.out.println(findMissingNumber(new int[]{2, 4}));
        System.out.println(findMissingNumber(new int[]{2, 3, 1, 5}));
        System.out.println(findMissingNumber(new int[]{4, 7, 8, 6}));


    }
    //TASK01
    public static int findClosestDistance(int[] arr) {
        if (arr.length < 2) return -1;
        else {
            Arrays.sort(arr);
            int dist = Integer.MAX_VALUE;

            for (int i = 1; i < arr.length - 1; i++) {
               int currentDist = arr[i+1] - arr[i];
               dist = Math.min(currentDist, dist);
                if (dist > (arr[i + 1] - arr[i])) dist = arr[i + 1] - arr[i];

            }//END OF LOOP
            return dist;
        }//END OF ELSE


    }//END OF METHOD TASK 1

    //TASK02
    public static int findSingleNumber(int[] arr){

        int unique = 0;
        //Arrays.sort(arr);
        if (arr.length < 2) return arr[0];
        else {
            for (int i = 0; i < arr.length-1; i++) {
                if (arr[i] != arr[i+1]) unique = arr[i];
            }//END OF LOOP
        }//END OF ELSE
        return unique;
    }//END OF METHOD TASK2

    //TASK03
    public static char findFirstUniqueCharacter(String str){

        for(Character ch : str.toCharArray()) {
            if(str.indexOf(ch) == str.lastIndexOf(ch)) {
                return ch;
            }//END OF IF
        }//END OF LOOP
        return 0;
    }//END OF METHOD TASK03

     //TASK04
    public static int findMissingNumber(int[] arr) {
        Arrays.sort(arr);
        int missingNo = 0;
        for(int i = 0; i < arr.length; i++ ){
            if(arr[i + 1] - arr[i] > 1 ){
                missingNo = arr[i] + 1;
                break;
            }//END OF IF
        }//END OF LOOP
        return missingNo;
    }//END OF METHOD TASK04




}//END OF CLASS
