package stringMethods;

public class _11_Substring_Method {
    public static void main(String[] args) {
        /*
        return type
        returns a string
        non_static
        takes ints as args
         */

        String str = "I love java";

        String firstWord = str.substring(0,1); // second number is not included (same as str.charAt(0))
        System.out.println(firstWord);// I

        String secondWord = str.substring(2,6);
        System.out.println(secondWord);// love

        String thirdWord = str.substring(7,11);
        System.out.println(thirdWord);// java

        System.out.println(str.substring(7));//java starts with index 7 and goes all the way till the end
        System.out.println(str.substring(str.length()/2+1));// space +  java
    }
}
