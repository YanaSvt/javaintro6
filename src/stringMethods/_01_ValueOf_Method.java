package stringMethods;

public class _01_ValueOf_Method {
    public static void main(String[] args) {
        /*
1. valueOf is a return type method
2. String is a data type returned
3 Static because we call in with class name without creating an object
4. It has an argument for this case int
         */
        int num = 125;

        String numAsStr = String.valueOf(num);
        System.out.println(num + 5);
        System.out.println(numAsStr + 5);

        System.out.println(5 + num);
        System.out.println(5 + numAsStr);


    }
}
