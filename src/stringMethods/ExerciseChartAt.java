package stringMethods;

import utilities.ScannerHelper;

public class ExerciseChartAt {
    public static void main(String[] args) {
        String str = "techGlobal";

        System.out.println(str.charAt(4));//G
//Print last character on the string
        System.out.println(str.charAt(9));//l
        //last character of any string:
        System.out.println(str.charAt(str.length()-1));//l

        String book = ScannerHelper.getString();
        System.out.println(book.charAt(book.length()-1));



    }
}
