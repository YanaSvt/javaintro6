package stringMethods;

public class _15_isEmpty_Method {
    public static void main(String[] args) {
        /*
        return type
        returns boolean
        non-static
        no args taken
         */
        String emptyStr = "";
        String word = "Hello";
        System.out.println("first string is empty " + emptyStr.isEmpty());
        System.out.println("first string is empty " + word.isEmpty());
    }
}
