package stringMethods;

public class _02_Concat_Method {
    public static void main(String[] args) {
        /*
1. Return type
2. It returns a string
3. Non-static because it is called by object name not a class name
4. It is taking a string argument
         */
        String str1 = "Tech";
        String str2 = "Global";

        System.out.println(str1.concat(str2));
        System.out.println("Tech".concat("Global"));

    }
}
