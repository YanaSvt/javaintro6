package stringMethods;

public class _09_Length_Method {
    public static void main(String[] args) {
        /*
        return
        returns int
        Non-static
        No args

         */
        String str = "TechGlobal";
        int lengthOfStr = str.length();

        System.out.println(lengthOfStr);//10

        String str2 = "";
        System.out.println(str2.length());//0

    }
}
