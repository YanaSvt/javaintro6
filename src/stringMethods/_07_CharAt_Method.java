package stringMethods;

public class _07_CharAt_Method {
    public static void main(String[] args) {
        /*
        1.Return type
        2.It returns a char
        3. Non-static
        4. It takes an int index as an argument

         */
        String name = "Yana";

        char firstLetter = name.charAt(0);
        char secondLetter = name.charAt(1);
        char thirdLetter = name.charAt(2);
        char fourthLetter = name.charAt(3);

        System.out.println(name);
        System.out.println(firstLetter);
        System.out.println(secondLetter);
        System.out.println(thirdLetter);
        System.out.println(fourthLetter);
    }
}
