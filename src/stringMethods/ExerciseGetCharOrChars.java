package stringMethods;

public class ExerciseGetCharOrChars {
    public static void main(String[] args) {
        //Not even name
        String name1 = "bilal";// length 5, middle is index of 2
        String name2 = "Matthew";//length 7, middle is index of 3
        String name3 = "Ronaldo!!";//length 9, middle is index of 4
        // to find the middle character we would need to do length - 1 divided by 2

        System.out.println(name1.charAt((name1.length()-1)/2));//l
        System.out.println(name1.charAt((name2.length()-1)/2));//l
        System.out.println(name1.charAt((name3.length()-1)/2));//l

        //For even name
        String name4 = "Yana";// length 4, middle character is index 1 and 2
        String name5 = "Yousef";

        System.out.println(name4.charAt(name4.length()/2 - 1) + "" +  name4.charAt(name4.length()/2));
        System.out.println(name5.charAt(name5.length()/2 - 1) + "" +  name5.charAt(name5.length()/2));

        int age = 45;
        String stringAge = "" + age;
        System.out.println(age);




    }
}
