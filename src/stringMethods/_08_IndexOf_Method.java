package stringMethods;

public class _08_IndexOf_Method {
    public static void main(String[] args) {
        /*
        Return type
        Returns an int
        Non-static
        Argument string
         */
        String str = "TechGlobal";
        System.out.println(str.indexOf("h"));//3

        System.out.println(str.indexOf('y'));// -1 basic int is negative one
        System.out.println(str.indexOf("Tech"));// 0 it does not give multiple answers
        System.out.println(str.indexOf("Global"));//4

        //lastIndexOf()

        System.out.println(str.indexOf('l'));//5 first l
        System.out.println(str.lastIndexOf('l'));//9 last l




    }
}
