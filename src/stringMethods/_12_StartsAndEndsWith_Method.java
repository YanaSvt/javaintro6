package stringMethods;

import java.util.Scanner;

public class _12_StartsAndEndsWith_Method {
    public static void main(String[] args) {
        /*
        return type
        returns boolean
        non-static
        takes string arg
         */
        String str = "TechGlobal";

        boolean startsWith = str.startsWith("T");
        boolean endsWith = str.endsWith("T");

        System.out.println(startsWith);
        System.out.println(endsWith);






    }
}
