package stringMethods;

public class MethodChaining {
    public static void main(String[] args) {
        String str = "TechGlobal";

        //single method
        System.out.println(str.toLowerCase());//techglobal

        //two methods chain
        System.out.println(str.toLowerCase().contains("tech"));//true but no other methods available since it is boolean

        //three methods chain
        System.out.println(str.toUpperCase().substring(4).length());// 6 last method gives int so no more methods here

        //4 methods chained
        String sentence = "Hello, my name is John Doe. I am 30 years old and I got to school at TechGlobal";
        System.out.println(sentence.toLowerCase().replace("a", "X").replace("i", "X").replace("o", "X"));

    }
}
