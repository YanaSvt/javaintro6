package stringMethods;

import utilities.ScannerHelper;

import java.util.Scanner;

public class Exercise02_length {
    public static void main(String[] args) {
        String book1 = ScannerHelper.getBook();
        Scanner input = new Scanner(System.in);


        System.out.println("Please enter your favorite quote");
        String favQuote = input.nextLine();

        System.out.println("The length of your favorite book is " + book1.length());
        System.out.println("The length of your favorite quote is " +favQuote.length());



    }
}
