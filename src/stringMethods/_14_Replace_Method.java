package stringMethods;

public class _14_Replace_Method {
    public static void main(String[] args) {
        /*
        return type
        returning a string
        non-static
        takes char or string args
         */
        String str = "ABC123";
        System.out.println(str);
        str = str.replace("ABC", "abc");
        System.out.println(str);

        System.out.println(str.replace("2", ""));
    }
}
