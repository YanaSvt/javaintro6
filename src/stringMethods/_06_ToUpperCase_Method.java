package stringMethods;

public class _06_ToUpperCase_Method {
    public static void main(String[] args) {
        /*
        1. Return type it is returning a value of something, if you put it inside a print or in a variable
        2. it returns a string
        3. Non-static
        4. Does not have args
         */

        System.out.println("HelloWorld".toLowerCase());//helloworld
        System.out.println("".toUpperCase());

        String s1 = "HELLO";
        String s2 = "hello";


        if(s1.toUpperCase().equals(s2.toUpperCase())) System.out.println("EQUAL");
        else System.out.println("NOT EQUAL");

    }
}
