package stringMethods;

public class _04_EqualsIgnoreCase_Method {
    public static void main(String[] args) {
        /*

         */

        String str1 = "Hello";
        String str2 = "Hi";
        String str3 = "hello";
        String str4 = "HeLlOHI";

        System.out.println(str1.equalsIgnoreCase(str2));//false
        System.out.println(str1.equalsIgnoreCase(str3));//true, if add a space to string it will give false
        System.out.println((str1 + str2).equals(str4));// false due to lower uppercase's
        System.out.println((str1 + str2).equalsIgnoreCase(str4));// true it ignores case

    }
}
