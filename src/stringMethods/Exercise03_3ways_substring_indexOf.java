package stringMethods;

public class Exercise03_3ways_substring_indexOf {
    public static void main(String[] args) {
        String str = "The best is Java";

        String lastWord = str.substring(12);
        System.out.println(lastWord);

        //second Way
        String lastW = str.substring(str.indexOf("Java"));
        System.out.println(lastW);

        //third way is a better way of doing it (it is dynamic to any string
        String lW = str.substring(str.lastIndexOf(' ') + 1); // or.trim() to remove extra space
        System.out.println(lW);

    }
}
