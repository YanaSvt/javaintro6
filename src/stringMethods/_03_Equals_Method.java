package stringMethods;

public class _03_Equals_Method {
    public static void main(String[] args) {
        /*
1. Return type, we are placing it inside a variable
2. It returns boolean
3. Non-static we did not use a classname we used an object which is variable
4. It takes objects as an argument which is a String in our case
         */

        String str1 = "Tech";
        String str2 = "Global";
        String str3 = "tech";
        String str4 = "TechGlobal";
        String str5 = "Tech";

        boolean  isEquals = str1.equals(str2);
        System.out.println(isEquals);//false

        System.out.println((str1.concat(str2)).equals(str4));// true left to right

        System.out.println(!str1.equals(str5));//false , without ! it is true but then ! converts it all to false
    }
}
