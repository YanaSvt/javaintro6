package stringMethods;

public class Exercise04_extractEveryWord_substring {
    public static void main(String[] args) {
       /* assume you have the string "I go to TechGlobal"
        extract every word from the string into other strings and print them out on different lines

        expected output:
        I
        go
        to
         TechGlobal*/

        String text = "I go to TechGlobal";

        String fW = "" + text.charAt(0);
        String sW = text.substring(2, 4);
        String tW = text.substring(5, 7);
        String lW = text.substring(8);


        System.out.println(fW);
        System.out.println(sW);
        System.out.println(tW);
        System.out.println(lW);


    }
}
