package stringMethods;

import utilities.ScannerHelper;

public class _13_Contains_Method {
    public static void main(String[] args) {
        /*
        return type
        returns boolean
        non-static
        takes a string as arg
         */

// write a program to ask the user for a string
        // return true if this string is a sentence and false if it is a single word


        String name = "John Doe";
        boolean containsJohn = name.toLowerCase().contains("John");
        System.out.println(containsJohn);

        String str = ScannerHelper.getString().trim();

        System.out.println(str.contains(" ") && str.endsWith("."));

    }
}
