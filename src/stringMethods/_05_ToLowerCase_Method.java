package stringMethods;

public class _05_ToLowerCase_Method {
    public static void main(String[] args) {
        /*
        If string is already lower case this method will not change it
        1. Return type
        2. Returns a String
        3. non-static method
        4. does not take args

         */
        String str1 = "JAVA IS FUN";

        System.out.println(str1);// JAVA IS FUN

        System.out.println(str1.toLowerCase());//java is fun

        char c = 'A';// char is not a String so we can't use it without converting into a string

        System.out.println(String.valueOf(c).toLowerCase());// 2 methods together is method chaining



    }
}
