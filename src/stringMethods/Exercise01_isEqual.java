package stringMethods;

import utilities.ScannerHelper;

public class Exercise01_isEqual {
    public static void main(String[] args) {
        String name1 = ScannerHelper.getString();
        String name2 = ScannerHelper.getString();

        //boolean  isEquals = name1.equals(name2);

       // if (isEquals == true) System.out.println("These strings are equal");
        //else System.out.println("These strings are not equal");

        if(name1.equals(name2)) System.out.println("These strings are equal");
        else System.out.println("These strings are not equal");

    }
}
