package stringMethods;

import java.util.Scanner;

public class Exercise05_startsWith_endsWith {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("please enter a word");
        String word = input.nextLine().toLowerCase();

        boolean startsWith = word.startsWith("a");
        boolean endsWith = word.endsWith("e");

        if (startsWith ==true && endsWith == true) System.out.println(true);
        else System.out.println(false);

        //other way
        System.out.println(word.startsWith("a") && word.endsWith("e"));
    }
}
