package arraylist_linkedlist_vector;

import java.util.ArrayList;
import java.util.Arrays;

public class _01_ArrayList_Introduction {
    public static void main(String[] args) {

        //1. How to create an Array vs ArrayList
        System.out.print("\n====================TASK1====================\n");
        String[] array = new String[3];//must define the size

        ArrayList<String> list = new ArrayList<>();// default capacity in memory is 10 if not assigned, but if print size it will give 0. Allows to update the size any time

        //2. How to get size of an Array and ArrayList
        System.out.print("\n====================TASK2====================\n");
        System.out.println("The ize of the array = " + array.length);//3
        System.out.println("The ize of the list = " + list.size());//0 because we did not assign the size yet

        //3. How to print an Array vs ArrayList
        System.out.print("\n====================TASK3====================\n");
        System.out.println("The array = " + Arrays.toString(array));//[null, null, null]
        System.out.println("The list = " + list); //[]

        //4. How to assign elements to anArray vs ArrayList
        System.out.print("\n====================TASK4====================\n");
        array[1] = "Alex";
        array[2] = "Max";
        array[0] = "John";
        System.out.println(Arrays.toString(array));

        list.add("Joe");
        list.add("Jane");
        list.add("Mike");
        list.add("Adam");// add will work if adding one by one, without skipping indexes
        //add in the middle
        list.add(2,  "Jazzy");
        System.out.println("The list = " + list); // order is first comes first served.

        //How to update an existing element In an Array vs ArrayList
        System.out.print("\n====================TASK5====================\n");
        array[1] = "Ali";
        System.out.println("The array = " + Arrays.toString(array));

        list.set(1, "Jasmine");//is updating the element that exist, it will not work if index does not exist
        System.out.println("The list = " + list);

        //6. How to retrieve or get an element from Array vs ArrayList
        System.out.print("\n====================TASK6====================\n");
        System.out.println(array[2]);//Max

        System.out.println(list.get(3));//Mike

        //7. How to Loop an Array vs ArrayList
        System.out.println("\n====================TASK7==fori loop==================\n");
        for (int i = 0; i < array.length; i++) {
            System.out.println(array[i]);
        }

        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }
        System.out.println("\n====================TASK7==for each loop==================\n");
        for(String element : array){
            System.out.println(element);
        }

        for(String  element : list){
            System.out.println(element);
        }


        System.out.println("\n-----forEach() method-----\n");
        list.forEach(System.out::println);
    }
}
