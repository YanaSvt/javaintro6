package arraylist_linkedlist_vector;

import java.util.*;

public class Practice06 {
    public static void main(String[] args) {
        System.out.print("\n====================TASK1====================\n");
        System.out.println(Arrays.toString(double1(new int[]{3, 2, 5, 7, 0})));

        System.out.print("\n====================TASK2====================\n");
        System.out.println(secondMax(new ArrayList<>(Arrays.asList(2, 3, 7, 1, 1, 7, 1))));//3

        System.out.print("\n====================TASK3====================\n");
        System.out.println(secondMin(new ArrayList<>(Arrays.asList(2, 3, 7, 1, 1, 7, 1))));//2

        System.out.print("\n====================TASK4====================\n");
        System.out.println(removeEmpty(new ArrayList<>(Arrays.asList("Tech", "Global", "", null , "", "School"))));

        System.out.print("\n====================TASK5====================\n");
        System.out.println(remove3orMore(new ArrayList<>(Arrays.asList(200, 5, 100, 99, 101, 75))));
        System.out.println(remove3orMore(new ArrayList<>(Arrays.asList(-12, -123, -5, 1000, 500, 0))));

        System.out.print("\n====================TASK6====================\n");
        System.out.println(uniquesWords("TechGlobal School"));
        System.out.println(uniquesWords("Star Light Star Bright"));

    }

    //TASK01
    /*
    Write a method called as
double
 to double each element
in an int array and return it back.
NOTE: The return type is an array.
Test data 1:
{3, 2, 5, 7, 0}
     */
    public static int[] double1(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            arr[i] = arr[i] * 2;
        }
        return arr;
    }

    //TASK02
    /*
    Write a method called as
secondMax
 to find and return
the second max number in an ArrayList
Test data 1:
{2, 3, 7, 1, 1, 7, 1}
     */
    public static int secondMax(ArrayList<Integer> list) {
        /*int max = Integer.MIN_VALUE;
        int secondMax = Integer.MIN_VALUE;

        for (Integer n : list) {
            if (n > max) {
                secondMax = max;
                max = n;
            }
        }
        return secondMax;*/

        Collections.sort(list);
        for (int i = list.size()-2; i >= 0 ; i--) {
            if(list.get(i) < list.get(list.size() - 1)) return list.get(i);
        }
        return 0;
    }
    //TASK03
    /*
Write a method called as secondMin to find and return
        the second min number in an ArrayList
        Test data 1:
        {2, 3, 7, 1, 1, 7, 1}
        Expected output 1:
        2
     */
    public static int secondMin(ArrayList<Integer> list){
        Collections.sort(list);
        for (int i = 1; i < list.size(); i++) {
            if(list.get(i) > list.get(0)) return list.get(i);
        }
        return 0;
    }
    //TASK04
    /*
    Write a method called as
removeEmpty
 to find and
remove all the elements in an ArrayList that are empty or
null.
     */
    public static ArrayList<String> removeEmpty(ArrayList<String> list){

        ArrayList<String> modif = new ArrayList<>();
        for (String str: list) {
            if (str == null) continue;
            else if (!str.isEmpty()) modif.add(str);

        }
        return modif;
        //Way2
       // list.removeIf(e -> e == null || e.isEmpty());// this method can remove null
       // return list;
    }
    //TASK05
    /*
    Write a method called as
remove3orMore
 to find and remove all
the elements in an ArrayList that are more than 2 digits.
Then, return the modified ArrayList back.
NOTE: - sign should not counted as a digit when it is negative
number.
Test data 1:
[200, 5, 100, 99, 101, 75]
Expected output 1:
[5, 99, 75]
Test data 2:
[-12, -123, -5, 1000, 500, 0]
Expected output 2:
[-12, -5, 0]
     */
    public static ArrayList<Integer> remove3orMore(ArrayList<Integer> list){
       // list.removeIf(e -> Math.abs(e) > 99);// this method can remove null
       //return list;
        //2nd way
        ArrayList<Integer> lr = new ArrayList<>();
        for (Integer n : list) {
            if(Math.abs(n) < 100) lr.add(n);
        }
        return lr;
    }

    //TASK06
    /*
    Write a method called as
uniquesWords
 to find and return all the
unique words in a String.
NOTE: The return type is an ArrayList.
NOTE: Assume that you will not be given extra spaces.
     */
    public static ArrayList<String> uniquesWords(String str){
      ArrayList <String> strAsList = new ArrayList<>();
      String[] arr = str.split(" ");
        for (String s: arr) {
           if(!strAsList.contains(s)) strAsList.add(s);
        }
        return strAsList;

    }

}