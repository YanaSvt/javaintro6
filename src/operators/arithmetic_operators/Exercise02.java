package operators.arithmetic_operators;

public class Exercise02 {
    public static void main(String[] args) {
        double salary = 120000;
        System.out.println("Monthly = " + salary / 12);
        System.out.println("Weekly = " + salary / 52);
        System.out.println("Bi-weekly = " + salary / 26);
    }
}
