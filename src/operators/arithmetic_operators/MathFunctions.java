package operators.arithmetic_operators;

public class MathFunctions {
    public static void main(String[] args) {
        int num1 = 9, num2 = 3;
        //System.out.println(num1+num2);//12
        int sum = num1 + num2;
        int multiplication = num1 * num2;
        int subtraction= num1 - num2;
        int division = num1 / num2;
        int remainder = num1 % num2;
        System.out.println("The sum is " + sum);
        System.out.println("The multiplication is " + multiplication);
        System.out.println("The subtraction is " + subtraction);
        System.out.println("The division is " + division);
        System.out.println("The remainder is " + remainder);

    }
}
