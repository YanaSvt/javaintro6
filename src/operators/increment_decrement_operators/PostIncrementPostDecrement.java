package operators.increment_decrement_operators;

public class PostIncrementPostDecrement {
    public static void main(String[] args) {
        System.out.print("\n====================TASK1====================\n");

        int num1 = 10, num2 = 10;
        System.out.println(num1++);//10 increase it by one, but not in this lane (next time)
        System.out.println(num1);//11
        System.out.println(++num2);//11 increase it by one in this line

        System.out.print("\n====================TASK2====================\n");
        int n1 = 5, n2 = 7;
        n1++;//keep it as 5 but next use increase by one
        n1 += n2;// 6+ 7 =13
        System.out.println(n1);

        System.out.print("\n====================TASK3====================\n");
        int i1 = 10;
        --i1;//decrease by 1 right now
        i1--;//decrease by 1 next use, printing is already usage so it will be 8
        System.out.println(--i1);//and it's decreasing again so it is 7

        System.out.print("\n====================TASK4====================\n");
        int number1 = 50;
        number1 -= 25;//25
        number1 -= 10;//15
        System.out.println(number1--);//still 15 but will be decreased next line

        System.out.print("\n====================TASK5====================\n");
        int i = 5;
        int age = 10 * i++; //50 it is multipliying by 5 because 6 will be in the next use
        System.out.println(age);//here we never used anymore i, so it stays 50

        System.out.print("\n====================TASK6====================\n");
        int var1 = 27;
        int result = --var1 / 2;//13
        System.out.println(++result);//14


    }
}
