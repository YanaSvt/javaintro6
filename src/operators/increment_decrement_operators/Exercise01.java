package operators.increment_decrement_operators;

import java.util.Scanner;

public class Exercise01 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int num1, num2 = 1;

        System.out.println("Please enter a number");
        num1 = input.nextInt();
        System.out.println("Your number is" +  num1);
        System.out.println(num1 + " * " + num2 + " = " + num1 * num2);//13
        System.out.println(num1 + " * " + num2 + " = " + num1 * ++num2);//26
        System.out.println(num1 + " * " + num2 + " = " + num1 * ++num2);//39
        System.out.println(num1 + " * " + num2 + " = " + num1 * ++num2);//52
        System.out.println(num1 + " * " + num2 + " = " + num1 * ++num2);//65

    }
}
