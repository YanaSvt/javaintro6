package operators.shorthand_operators;

import java.util.Scanner;

public class Exercise02 {
    public static void main(String[] args) {

        System.out.print("\n====================Exercise02====================\n");

        Scanner scanner = new Scanner(System.in);
        double balance, first_tr, second_tr, third_tr;

        System.out.println("Please enter your balance");
        balance= scanner.nextDouble();
        System.out.println("Initial balance is $" +  balance);

        System.out.println("Please enter your 1st transaction");
        first_tr = scanner.nextDouble();

        balance -= first_tr;
        System.out.println("Balance after first transaction is " + balance);

        System.out.println("Please enter your second transaction");
        second_tr = scanner.nextDouble();
        balance -= second_tr;
        System.out.println("Balance after second transaction is " + balance);

        System.out.println("Please enter your third transaction");
        //third_tr = scanner.nextDouble();
        //balance -= third_tr;
        System.out.println("Balance after third transaction is " + (balance -= scanner.nextDouble())); //Fewer steps, this way is preferred



    }
}
