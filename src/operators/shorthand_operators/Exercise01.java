package operators.shorthand_operators;

public class Exercise01 {
    public static void main(String[] args) {
        int a = 8;
        int b = 3;

        System.out.println(a + b);//11
        a += 2;
        System.out.println(a + b);//13
        b -= 3;
        System.out.println(a + b);//10
        a *= 2;
        b += 5;
        System.out.println(a + b);//25
        a /= 2;
        b -= 1;
        System.out.println(a + b);//14
        a %= 4;
        b %= 3;
        System.out.println(a + b);//3

    }
}
