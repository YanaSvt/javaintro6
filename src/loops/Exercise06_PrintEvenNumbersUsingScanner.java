package loops;

import utilities.ScannerHelper;

public class Exercise06_PrintEvenNumbersUsingScanner {
    public static void main(String[] args) {
        /*
Write a program that asks user to enter 2 different positive numbers
Print all the even numbers bt given numbers by user in ascending order
3, 10 ->
4
6
8
10

7, 2  ->
2
4
6
start point: Math.min(first, second)
end point: Math.max(first, second)
update: increment

 */

        int start = ScannerHelper.getNumber();
        int end = ScannerHelper.getNumber();
        int min = Math.min(start, end);
        int max = Math.max(start, end);

        for (int num = min; num <= max ; num++) {
           if (num % 2 == 0) System.out.println(num);

        }

        // or another way
      //  for (int i = Math.min(first, second); i <= Math.max(first, second); i++) {
       //     if(i % 2 == 0) System.out.println(i);
     //   }
 /*
        if(i1 >= i2){
            for(int j = i2; j <= i1; j++){
                if(j % 2 == 0){
                    System.out.println(j);
                }
            }
        }
        else{
            for(int j = i1; j <= i2; j++){
                if(j % 2 == 0){
                    System.out.println(j);
                }
            }
        }
         */



    }
}
