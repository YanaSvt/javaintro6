package loops;

public class UnderstandingLoops {
    public static void main(String[] args) {

        /*
        for loop syntax
        for (initialization; termination; update){
        //lock of code to be executed
        }

        initialization -> starts point.
        termination -> stop point
        update -> increasing decreasing
         */
        //Print hello world 20 times
        for(int num = 0; num < 20; num ++){
            System.out.println("Hello World!");
        }






    }
}
