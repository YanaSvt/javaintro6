package loops;

import utilities.ScannerHelper;

public class Exercise10_SumOfNumbersByUser {
    public static void main(String[] args) {
     /*
Write a program that asks user to enter 5 numbers
Find sum of the given numbers by user.

2, 3, 4, 5, 6

Output:
20

11, 15, 23, -7, 8

Output:
50
 */

        System.out.println("\n---------with for loop----------\n");
        int sum = 0;

        for (int i = 1; i <= 5; i++) { // 1, 2, 3, 4, 5
            sum += ScannerHelper.getNumber();
        }

        System.out.println(sum);

        System.out.println("\n---------with while loop----------\n");


        int start = 1;
        int sumWhile = 0;

        while(start <= 7){
            sumWhile += ScannerHelper.getNumber();
            start++;
        }

        System.out.println(sumWhile);












    }
}
