package loops;

import utilities.ScannerHelper;

public class Exercise12_ReverseString {
    public static void main(String[] args) {
/*
        Write a program that reads a name from user
        Reverse the name and print it back
        Test data:
        James
        Expected Output:
        semaJ
        Test data:
        John
        Expected Output:
        nhoJ
          */

//First way
       /* String s = ScannerHelper.getFirstName();
        for (int i = s.length()-1; i >= 0; i--) {
            System.out.print(s.charAt(i));
        }*/
//Second way
        String name = ScannerHelper.getFirstName();

        String reversedName = "";

        for(int i = name.length()-1; i >= 0; i--){
            reversedName += name.charAt(i);
        }

        System.out.println(reversedName);



    }
}
