package loops.practices;

import utilities.ScannerHelper;

public class Exercise04_CountVowelsInString {
    public static void main(String[] args) {
        /*
        Write a program that asks user to enter a String.
Count the number of vowels in the given String and print it.
Vowels are A, E, O, U, I, a, e, o, u, I
         */
        String str = ScannerHelper.getString();
        int vowelCounter = 0;

        for (int i = 0; i < str.length(); i++) {
            char c = Character.toUpperCase(str.charAt(i));

            if(c == 'A' || c == 'E' || c == 'I' || c == 'O' || c == 'U') vowelCounter++;
        }
        System.out.println(vowelCounter);


    }
}
