package loops;

public class Exercise03_PrintEvenNumbers {
    public static void main(String[] args) {
/*
print even numbers ascending 0 to 10
start: 0
end: 10:
update: increment
 */
        //this method is not recommended since it is dangerous in terms of the requirement change
        for (int num = 0; num <= 10; num += 2) {
            System.out.println(num);
        }
        //another way
        for (int i = 0; i <= 10; i++) {
            if (i % 2 == 0) System.out.println(i);
        }


//another way not preferred as well (better to use numbers from reqs
        for (int a = 0; a <= 5; a++) {
            System.out.println(a * 2);
        }


    }
}
