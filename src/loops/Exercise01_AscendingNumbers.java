package loops;

public class Exercise01_AscendingNumbers {
    public static void main(String[] args) {
       /*
Write a Java program to print numbers from 1 to 10 (1 and 10 are included)
Expected output: start 1, end 10, it is increment
1
2
.
.
.
8
9
10

 */
        for(int num = 1; num <= 10; num ++){
            System.out.println(num);
        }
    }
}
