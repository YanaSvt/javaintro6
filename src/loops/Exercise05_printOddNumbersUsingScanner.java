package loops;

import utilities.ScannerHelper;

public class Exercise05_printOddNumbersUsingScanner {
    public static void main(String[] args) {
        /*
        Write a Java program to ask user to enter a number and print
all the odd numbers starting from 0 to given number by user
(0 and given number is included)
it depends if user gives positive or negative number, so need to ask . This case is for positive
start 0, end usernumber, increment
         */


        int i = ScannerHelper.getNumber();

        for (int num = 0; num <= i ; num++) {
            if (num % 2 ==1)System.out.println(num);
        }







    }
}
