package loops;

public class Exercise04_PrintNumbersDivisibleBy5 {
    public static void main(String[] args) {
        /*
        Write a program to print numbers 1 to 50 only divisible by 5
        start: 1
        end: 50
        update: increment

         */
        for (int num = 1; num <= 50; num++) {
            if (num % 5 == 0) System.out.println(num);
        }









    }
}
